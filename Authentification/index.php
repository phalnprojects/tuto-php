<?php
session_start([
    'cookie_httponly' => true,
]);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Hello!</title>
	<link rel="stylesheet" href="../css/normalize.css">
	<link rel="stylesheet" href="../css/styles.css">
    </head>
    <body>
	<h1>Hello !!</h1>
	<?php if(isset($_SESSION['authentification']) && ($_SESSION['authentification'] === TRUE)): ?>
	<p>Bonjour <?php echo $_SESSION['prenom'] ?></p>
	<p><a href="fin_session.php">Déconnexion</a></p>
	<?php else: ?>
	<p>Pour vous authentifier: <a href="form_identif.php">c'est par ici</a></p>
	<?php endif; ?>
    </body>
</html>
