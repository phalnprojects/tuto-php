<?php
/**
 * Une autre façon de faire un autoload des classes.
 * Le tableau $possibilités contient les différents chemins et extensions possibles
 */
spl_autoload_register(function ($className) {
	$base = 'inc/';
	$extension = '.class.php';
    $possibilites = array(
        $base.$className.$extension,
        $className.$extension
    );
    foreach ($possibilites as $fichier) {
        if (file_exists($fichier)) {
            require_once($fichier);
            return true;
        }
    }
    return false;
});

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function dumpVar($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}


function genHash(array $mdpClair)
{
    foreach ($mdpClair as $value) {
	$pass[$value] = password_hash($value, PASSWORD_DEFAULT);
    }
    return $pass;
}