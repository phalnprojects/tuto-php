<?php
include_once('inc/connect.inc.php');

/**
 * Classe encapsulant sous forme de singleton une connexion PDO � la base de donn�es.
 * Elle ne peut pas �tre d�riv�e.
 */
/**
 * Classe encapsulant sous forme de singleton une connexion MySQLi à la base de données.
 * Elle ne peut pas être dérivée.
 */
final class BDD
{
    private static $_bdd = null;	//  L'instance singleton
    private $connexion;			//  La connexion MySQLi ou PDO
    public static $infoBdd;		//  Les informations de la base de données. Voir connect.inc.php

    /**
     *	Constructeur privé, singleton oblige...
     */
    private function __construct()
    {
	switch (strtoupper(BDD::$infoBdd['interface'])) {
	    case 'PDO':
		$dsn = BDD::$infoBdd['type'].':host='.BDD::$infoBdd['host'].';dbname='.BDD::$infoBdd['dbname'];
		$this->connexion = new PDO($dsn, BDD::$infoBdd['user'], BDD::$infoBdd['pass'], array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		break;
	    case 'MYSQLI':
		//  Instanciation de la connexion MySQLi pour initialiser l'attribut $_connexion
		$this->connexion = new mysqli(BDD::$infoBdd['host'], BDD::$infoBdd['user'], BDD::$infoBdd['pass'], BDD::$infoBdd['dbname']);
		if($this->connexion->connect_error)
		    throw new Exception("Pb instanciation Bdd: {$this->connexion->connect_error}.");
		else
		    $this->connexion->set_charset("utf8");
		break;
	    default:
		    throw new Exception("Interface Bdd non prise en charge.");
		break;
	}
    }

    /**
     * L'accès à l'instance unique.
     * @return BDD l'instance singleton.
     */
    public static function get_bdd()
    {
	// Si l'instance n'existe pas, on exé&cute le constructeur pour la créer
        if (is_null(self::$_bdd))
	{
            self::$_bdd = new self;
        }

	//  On retourne l'instance existante, qui vient d'être créée ou qui existait déjà.
        return self::$_bdd;
    }

	/**
	 *  __clone vide pour s'assurer de ne pas pouvoir créer de copie du singleton
	 */
    private function __clone()
	{}

    /**
     * Lecteur de la connexion PDO
     * @return PDO l'objet PDO accèdant à la base de données.
     */
    public function get_connexion()
    {
        return $this->connexion;
    }
    
    /**
     * Lecteur statique de la connexion à la bdd (PDO ou MySQLi)
     * @return PDO ou MySQLi l'objet accèdant à la base de données.
     */
    public static function getConnexion()
    {
        return self::get_bdd()->connexion;
    }
}
