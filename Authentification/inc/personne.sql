-- phpMyAdmin SQL Dump
-- version 4.4.15.8
-- https://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 07 Décembre 2016 à 23:15
-- Version du serveur :  5.6.31
-- Version de PHP :  7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `diverscourstp`
--

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `id_personne` int(10) unsigned NOT NULL,
  `login` varchar(25) NOT NULL COMMENT 'Le login',
  `mdp` varchar(255) NOT NULL COMMENT 'Le password hasché par php',
  `prenom` varchar(45) DEFAULT NULL,
  `mdpEnClair` varchar(255) DEFAULT NULL COMMENT 'A NE JAMAIS FAIRE!! Existe juste pour le cours afin de ne pas oublier le mdp!'
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `personne`
--

INSERT INTO `personne` (`id_personne`, `login`, `mdp`, `prenom`, `mdpEnClair`) VALUES
(1, 'p.alluin', '$2y$12$qYJeqM/v1anwlM4j6Sg9OOTbOzcnX3KV6gliGvhKGQoTtXVfBgrFu', 'Philippe', 'Alluin'),
(2, 'a.turing', '$2y$12$qWdmt43nZoPpEQ3POyzktOziYQr0mEDukGNK.qKDJ0e7lAAsu9mrK', 'Alan', 'Turing');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`id_personne`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `id_personne` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
