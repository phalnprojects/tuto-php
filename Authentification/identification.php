<?php
session_start();

define(DUMP, false);

if('POST' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS))
{
    require_once 'inc/fct_utiles.inc.php';
    
    if(DUMP) var_dump ($_POST);

    //  ATTENTION!!!
    //  Pensez à faire les contrôles d'existence et de validité des paramètres...
    $arg = array(
	    'login' => FILTER_SANITIZE_SPECIAL_CHARS,
	    'mdp' => FILTER_SANITIZE_SPECIAL_CHARS,
    ); 
    $postFiltre = filter_input_array(INPUT_POST, $arg);
    if(DUMP) var_dump ($postFiltre);

    if($postFiltre['login'] != '' && $postFiltre['mdp'] != '')
    {
	//  Connexion à la BDD (voir connect.inc.php)
	$bdd = BDD::get_bdd()->get_connexion();

	// Inutile de quoter les données du formulaire composant les requêtes
	// puisqu'on a utilisé filter_input.
	// Dans le cas contraire, pensez-y !
	$login = $postFiltre['login'];

	$query = "SELECT * FROM personne WHERE (login='" . $login . "')";
	if(DUMP) var_dump ($query);
	
	$res = $bdd->query($query);

	if($res->rowCount() == 1)
	{
	    //  On a quelqu'un du login fourni, on va vérifier le mdp
	    $row = $res->fetch(PDO::FETCH_ASSOC);
	    $mdpHash = $row['mdp'];
	    
	    if(DUMP) var_dump(password_get_info($mdpHash));

	    //  On vérifie le mdp de la bdd avec celui du formulaire
	    if(password_verify($postFiltre['mdp'], $mdpHash))
	    {
		//  L'utilisateur est authentifié
		$_SESSION['prenom'] = $row['prenom'];
		$_SESSION['authentification'] = TRUE;
		if(!DUMP) header("location: pageAvecAuthentification.php");
	    }
	    else
	    {
		//  Mauvaise authentification, redirection vers le formulaire
		echo'Mauvaise authentification';
		if(!DUMP) header("Refresh: 2;URL=form_identif.php");
	    }
	}
	else
	{
	    //  Mauvais identification, redirection vers le formulaire
	    echo'Mauvaise authentification';
	    if(!DUMP) header("Refresh: 2;URL=form_identif.php");
	}
    }
}
else
{
    if(!DUMP) header("location: index.php");
}
