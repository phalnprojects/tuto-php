<?php
define(DUMP, FALSE);

if('POST' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS))
{
    require_once 'inc/fct_utiles.inc.php';
    if(DUMP) var_dump ($_POST);

    //  ATTENTION!!!
    //  Pensez à faire les contrôles d'existence et de validité des paramètres...
    $arg = array(
	    'login' => FILTER_SANITIZE_SPECIAL_CHARS,
	    'mdp' => FILTER_SANITIZE_SPECIAL_CHARS,
	    'commentaires' => FILTER_SANITIZE_SPECIAL_CHARS,
    ); 
    $postFiltre = filter_input_array(INPUT_POST, $arg);
    if(DUMP) var_dump ($postFiltre);

    if($postFiltre['login'] != '' && ($postFiltre['mdp'] != ''))
    {
	$options = [ 'cost' => 12, ];
	$motDePasse = password_hash($postFiltre['mdp'], PASSWORD_DEFAULT, $options);	// $motDePasse est ce qui est conservé dans la BdD...
	if(DUMP) dumpVar($motDePasse);

	//  Enregistrement dans la bdd.
	//  ATTENTION!!! Pensez à faire les contrôles d'existence des paramètres...
	//  et le quote si vous ne préparez pas la requête ou si vous n'utilisez pas filter_input...
	$bdd = BDD::get_bdd()->get_connexion();
	$query = "INSERT INTO personne (login, mdp, mdpEnClair) ". 
			"VALUES ('".$postFiltre['login']."', '".$motDePasse."', '".$postFiltre['commentaires']."')";
	if(DUMP) dumpVar($query);
	$bdd->exec($query);
    }
}
if(!DUMP) header("location: index.php") ;