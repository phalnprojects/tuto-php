<?php
session_start([
    'cookie_httponly' => true,
]);
require_once 'inc/fct_utiles.inc.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Bravo!!</title>
	<link rel="stylesheet" href="../css/normalize.css">
	<link rel="stylesheet" href="../css/styles.css">
    </head>
    <body>
	<?php if(isset($_SESSION['authentification']) && (TRUE === $_SESSION['authentification'])): ?>
	<p>Bonjour <?php echo $_SESSION['prenom'] ?></p>
	<p>Votre numéro de session est: <?php echo session_id(); ?></p>
	<?php dumpVar($_SESSION); ?>
	<p><br/><a href="fin_session.php">Déconnexion</a></p>
	<?php else: ?>
	<p>Vous devez être authentifié pour voir cette page ...</p>
	<?php
	header ("Refresh: 2;URL=form_identif.php");
	endif ?>
    </body>
</html>
