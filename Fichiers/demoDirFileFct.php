<?php
function dump_var($var)
{
    echo'<pre>';
    var_dump($var);
    echo'</pre>';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
	<link rel="stylesheet" media="all" href="css/normalize.css" />
	<link rel="stylesheet" media="screen" href="css/styles.css" />
        <title>Démos Dir</title>
    </head>
    <body>
	<?php
	    $dirBase = '../Fichiers';
	?>
	
	<h1>Fonction scandir()</h1>
	<?php
	$dirScan = scandir($dirBase);
	echo"<p>Scan de $dirBase:</p>"; dump_var($dirScan);
	?>
	
	<h1>Fonction is_dir()</h1>
	<?php
	foreach ($dirScan as $name) {
	    $msg = (is_dir($dirBase.'/'.$name)) ? "$name est un dossier." : "$name n'est pas un dossier.";
	    echo "<p>$msg</p>";
	}
	?>
	
	<h1>Fonctions opendir(), readdir(), is_file(), mime_content_type()</h1>
	<?php
	$dirName = $dirBase.'/inutiles/';
	$dir = opendir($dirName);
	while($file = readdir($dir))
	{
	    if('.' != $file && '..' != $file && is_file($dirName.$file))
	    {
		$type = mime_content_type($dirName.$file);
		$msg = "$file est de type $type";
	    }
	    else
	    {
		$msg = $file;
	    }
	    echo "<p>$msg</p>";
	}
	?>
	
	<h1>Fonction pathinfo()</h1>
	<?php
	$fileName = $dirBase.'/inutiles/logoOrt.png';
	$pathInfo = pathinfo($fileName);
	echo"<p>$fileName:</p>"; dump_var($pathInfo);
	
	$fileName = $dirBase.'/inutiles';
	$pathInfo = pathinfo($fileName);
	echo"<p>$fileName:</p>"; dump_var($pathInfo);
	
	$fileName = '.gitignore';
	$pathInfo = pathinfo($fileName);
	echo"<p>$fileName:</p>"; dump_var($pathInfo);
	?>
	
	<h1>Création d'un fichier et écriture de texte</h1>
	<?php
	$fileName = $dirBase.'/inutiles/testEcritureFichier.txt';
	//  Ouverture en création. !! écrase le fichier existant...
	if ($fd = fopen($fileName, 'w'))
	{
	    $date = date('Y-m-d H:i:s');
	    $somecontent = "Création et écriture de texte dans le fichier le $date\r\n";
	    if (FALSE === fwrite($fd, $somecontent))
	    {
		echo "<p class=\"bad\">Impossible d'écrire dans le fichier ($fileName)<p>";	
	    }
	    else
	    {
		echo "<p class=\"bon\">Création et écriture dans le fichier ($fileName) OK<p>";	
	    }
	    fclose($fd);
	}
	else
	{
	    echo "<p class=\"bad\">Impossible d'ouvrir le fichier ($fileName)</p>";
	}
	
	//  Ouverture en ajout à la fin du fichier
	if ($fd = fopen($fileName, 'a+'))
	{
	    for($i=1; $i < mt_rand(1, 11); $i++)
	    {
		$date = date('Y-m-d H:i:s');
		$somecontent = "Ajout de texte n° $i dans le fichier le $date\r\n";
		if (FALSE === fwrite($fd, $somecontent))
		{
		    echo "<p class=\"bad\">Impossible d'ajouter dans le fichier ($fileName)<p>";	
		}	    
		else
		{
		    echo "<p class=\"bon\">Ajout dans le fichier ($fileName) OK<p>";	
		}
	    }
	    fclose($fd);
	}
	else
	{
	    echo "<p class=\"bad\">Impossible d'ouvrir le fichier ($fileName)</p>";
	}
	?>
	
	<h1>Lecture ligne par ligne d'un fichier texte</h1>
	<?php
	if($fd = fopen($fileName, 'r'))
	{
	    $i = 0;
	    while (!feof($fd))
	    {
		$ligne = fgets($fd);
		$mots = explode(' ', $ligne);
		if('Ajout' == $mots[0])
		    $i++;
	    }
	    echo "<p>Il y a $i fois le mot 'Ajout' dans le fichier $fileName</p>";
	}
	else
	{
	    echo "<p class=\"bad\">Impossible d'ouvrir le fichier ($fileName)</p>";
	}
	?>
	
	<h1>Lecture en bloc d'un fichier texte</h1>
	<?php
	$i = 0;
	$lignes = file($fileName);
	foreach ($lignes as $ligne)
	{
	    $mots = explode(' ', $ligne);
	    if('Ajout' === $mots[0])
		$i++;
	}
	echo "<p>Il y a $i fois le mot 'Ajout' dans le fichier $fileName</p>";
	?>
	
	<h1>Lecture d'un fichier csv</h1>
	<?php
	$fileName = $dirBase.'/inutiles/helloC.csv';
	if($fd = fopen($fileName, 'r'))
	{
	    $i = 0;
	    while(FALSE !== ($ligne = fgetcsv($fd, 0, ';')))
	    {
		echo '<p>Lignes ' . $i++ .': </p>'; dump_var($ligne);
	    }
	}
	else
	{
	    echo "<p class=\"bad\">Impossible d'ouvrir le fichier ($fileName)</p>";
	}
	?>
	
    </body>
</html>
