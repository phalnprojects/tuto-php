<?php 
	setcookie('DemoCookieToujours','Ce cookie est actif',time()+60);
	
	if (true === filter_input(INPUT_POST, 'submitted', FILTER_VALIDATE_BOOLEAN))
	{
	    $saisie = ($saisie= filter_input(INPUT_POST, 'saisie', FILTER_SANITIZE_SPECIAL_CHARS)) ? $saisie : 'Vide';
	    setcookie('DemoCookie','Le cookie DemoCookie est actif: '.$saisie,time()+20);
	    header('location: demo_cookies.php') ;
	}
	
	//  on utilise les cookies, s'ils existent.
	$DemoCookie = ($DemoCookie = filter_input(INPUT_COOKIE,'DemoCookie', FILTER_SANITIZE_SPECIAL_CHARS)) ? $DemoCookie : "Rien";
	$ToujoursCookie = ($ToujoursCookie = filter_input(INPUT_COOKIE,'DemoCookieToujours', FILTER_SANITIZE_SPECIAL_CHARS)) ? $ToujoursCookie : "Toujours rien...";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Coockies en php</title>
    </head>
<body>
	<form action= "<?php  echo (filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL));  ?>" method ="post">
	    <fieldset><legend>Cookie:</legend>
		<input type="text" name="saisie">	
		<input type ="hidden" name="submitted" value="true" /><br/>
		<input type="submit" value="Envoyer" />
	    </fieldset>
	</form>
	<?php if(isset($_COOKIE['DemoCookie'])): ?>
		<p>Le cookie DemoCookie est défini: <?php echo $DemoCookie;?></p>
	<?php else: ?>
		<p>Le cookie DemoCookie n'est pas défini...<?php echo $DemoCookie;?></p>
	<?php endif; ?>
	<?php if(isset($_COOKIE['DemoCookieToujours'])): ?>
		<p>Le cookie DemoCookieToujours est défini: <?php echo $ToujoursCookie;?></p>
	<?php else: ?>
		<p>Le cookie DemoCookieToujours n'est pas défini...<?php echo $ToujoursCookie;?></p>
	<?php endif; ?>
</body>
</html>