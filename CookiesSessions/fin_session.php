<?php
    session_start(['cookie_httponly' => true,]) ;

    //  Détruit les variables de la session courante
    //session_unset();	//  Code ancien... Obsolète
    $_SESSION = array();

    //  Détruit le cookie de session
    if (ini_get('session.use_cookies')) {
	    $params = session_get_cookie_params();
	    setcookie(session_name(), '', time() - 42000,
		    $params['path'], $params['domain'],
		    $params['secure'], $params['httponly']
	    );
    }

    //  Détruit toutes les données associées à la session courante
    session_destroy() ;

    //  Redirection vers la page d'authentification
    header('location: demo_session.php') ;