<?php 
    //  La fonction suivante permet de rendre le cookie inaccessible en Javascript (dernier param httponly true)
    //  Valable uniquement pour le script, à placer avant session_start().
    //  void session_set_cookie_params ( int $lifetime 
    //					    [, string $path 
    //					    [, string $domain 
    //					    [, bool $secure = false 
    //					    [, bool $httponly = false ]]]] )
    //session_set_cookie_params ( 60, '/', 'localhost', false, true );
    session_start(['cookie_httponly' => true,]);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sessions en php</title>
    <script>
	function getCookie(sName)
	{
		var oRegex = new RegExp("(?:; )?" 
				    + sName
				    + "=([^;]*);?");

		if (oRegex.test(document.cookie))
			return decodeURIComponent(RegExp["$1"]);
		else
			return null;
	 }
    </script>
</head>
<?php
    if(isset($_POST['nom']))
    {
	//  Remplace l'identifiant de session courant par un nouveau 
	session_regenerate_id(TRUE);

	//  Place le nom arrivant en post dans la session.
	$_SESSION['nom'] = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS);
    }
?>
<body id="corps" onload="document.getElementById('nom').select()">
	<h1 class="titre">Gestion des sessions ...</h1>
	<p>Chemin des sessions: <?php echo session_save_path(); ?></p>	
	<p>session_get_cookie_params(): <?php echo var_dump(session_get_cookie_params()); ?></p>	
	<p> Id de session: <?php echo session_id(); ?></p>	
	<p>La variable $_COOKIE: <?php var_dump($_COOKIE); ?></p>	
	<p>La variable $_SESSION: <?php var_dump($_SESSION); ?></p>	
	<script>
		// alert(getCookie('PHPSESSID'));
	</script>
	<div id="cadre_saisie">
	    <form method="post" action="<?php echo (filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL)); ?>" id="formulaire">
		<fieldset>
		    <?php if(isset($_SESSION['nom'])): ?>
			    <legend>Bonjour <?php echo $_SESSION['nom'] ?></legend>
		    <?php else: ?>
			    <legend>Identifiez-vous</legend>
		    <?php endif; ?>
			    <input id="nom" type="text" name="nom" maxlength="8" size="10" value="<?php	if (isset($_SESSION['nom'])) {echo $_SESSION['nom']; } ?>" />
		    <br/>
		    <input type="submit" value="Identification" style="width:10pc" />
		</fieldset>
	    </form>
	</div>
	<div id="cadre_affichage">
	    <?php if(isset($_SESSION['nom'])): ?>
		    <p>Bonjour <?php echo $_SESSION['nom'] ?></p>
		    <p>Vous pouvez:</p>
		    <ul>
			<li><a href="autre_page_session.php">visiter cette autre page.</a></li>
			<li><a href="fin_session.php">vous d�connecter.</a></li>
		    </ul>
	    <?php else: ?>
		    <p>Identifiez-vous svp...</p>
	    <?php endif ?>
	</div>
</body>
</html>