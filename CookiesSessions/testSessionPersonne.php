<?php
//  Initialisation des sessions
session_start();

//  Décommentez pour vider et terminer la session
//$_SESSION = array();
//session_destroy();

echo'<p> Session avant:</p>';
var_dump($_SESSION);

$prochainIndex= (isset($_SESSION['$prochainIndex'])) ? $_SESSION['$prochainIndex'] : 0;
var_dump($prochainIndex);

//  Les données de bases pour créer un tableau "personne"
$personneAjouter = array(
    'prenom' => 'NomPers_'.$prochainIndex,
    'nom' => 'Prénom_'.$prochainIndex,
    'age' => 25+25*$prochainIndex,
);
var_dump($personneAjouter);

//  S'il y a plus de 10 éléments dans le tableau $_SESSION['tabPers'], on en efface le premier.
if(isset($_SESSION['tabPers']) && (count($_SESSION['tabPers']) >= 5))
{
    $aEffacer = array_key_first($_SESSION['tabPers']);
    unset($_SESSION['tabPers'][$aEffacer]);
}

//  On ajoute la personne créée
$_SESSION['tabPers'][] = $personneAjouter;

//  L'index du prochain ajout dans la session
$_SESSION['$prochainIndex'] = $prochainIndex + 1;

echo'<p> Session après:</p>';
var_dump($_SESSION);



foreach ($_SESSION['tabPers'] as $index => $personne) {
//	var_dump($index);
//	var_dump($personne);
    echo "Personne n° $index :<br/>";
    foreach ($personne as $key => $value) {
//	var_dump($key);
//	var_dump($value);
	echo "$key : $value, ";
    }
    echo "<br/><br/>";
}

