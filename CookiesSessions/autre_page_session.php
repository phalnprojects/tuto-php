<?php 
session_start(['cookie_httponly' => true,]);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sessions en php</title>
</head>

<body>
    <h1>Gestion des sessions ...</h1>	
    <div>
	<?php if(isset($_SESSION['nom'])): ?>
		<p>Bonjour <?php echo $_SESSION['nom'] ?></p>
		<p>Vous pouvez:</p>
		<ul>
			<li><a href="autre_page_session.php">visiter cette autre page.</a></li>
			<li><a href="fin_session.php">vous déconnecter.</a></li>
		</ul>
	<?php else: ?>
		<p>Identifiez-vous svp...</p>
	<?php endif ?>
    </div>
</body>
</html>