<?php 
    //  Accès en POST?
    if('POST' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS))
    {
	    $newbgColor=filter_input(INPUT_POST, 'bgColor', FILTER_SANITIZE_SPECIAL_CHARS);
	    $newtxtColor=filter_input(INPUT_POST, 'txtColor', FILTER_SANITIZE_SPECIAL_CHARS);

	    // bool setcookie ( string $name [, string $value [, int $expire = 0 [, string $path [, string $domain [, bool $secure = false [, bool $httponly = false ]]]]]] )
	    setcookie('bgColor',$newbgColor,time()+10);
	    setcookie('txtColor',$newtxtColor,time()+10);
    }

    //  Des coockies existent? alors on les utilise, sinon on utilise les valeurs par défaut
    $bgColor = (isset($_COOKIE['bgColor']))? filter_input(INPUT_COOKIE, 'bgColor', FILTER_SANITIZE_SPECIAL_CHARS): 'Black';
    $txtColor = (isset($_COOKIE['txtColor'])) ?  filter_input(INPUT_COOKIE, 'txtColor', FILTER_SANITIZE_SPECIAL_CHARS): 'White';
?>
<!-- HTML Page-->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Coockies en php</title>
    </head>
<body style="background-color:<?php echo $bgColor ?>; color:<?php echo $txtColor ?>;">
	<?php
		var_dump($bgColor);
		var_dump($txtColor);
	?>
	<form action= "<?php  echo (filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL));  ?>" method ="post">
		<fieldset>
		<fieldset><legend>Couleur du body:</legend>
		<select name="bgColor">
			<option value ="Red">Red</option>
			<option value ="Green" selected="selected">Green</option>
			<option value ="Blue">Blue</option>
			<option value ="Yellow">Yellow</option>
			<option value ="Black">Black</option>
			<option value ="Brown">Brown</option>
			<option value ="White">White</option>
		</select>
		</fieldset>
		<fieldset><legend>Couleur du texte:</legend>
		<select name="txtColor">
			<option value ="Red">Red</option>
			<option value ="Green">Green</option>
			<option value ="Blue">Blue</option>
			<option value ="Yellow">Yellow</option>
			<option value ="Black">Black</option>
			<option value ="Brown">Brown</option>
			<option value ="White" selected="selected">White</option>
		</select>
		</fieldset>
		<input type ="hidden" name="submitted" value="true" /><br/>
		<input type="submit" value="Envoyer" />
		</fieldset>
	</form>
	<p>Sollicitari referente illi uti contentum subinde est uti illi 
	ne hortaretur doctus quem Scutariis doctus ut doctus ut militare 
	eum ne adiumenta hortaretur uti perniciem referente ne ad venerit 
	Scutariis.</p>
</body>
</html>