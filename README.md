# Code snippets illustration du cours PHP

## Prérequis

Pour installer et utiliser cet ensemble de code snippet, il vous faut:
- Git (https://git-scm.com/)
- Apache
- PHP7 (7.42 minimum, 8.1 pour les démos du dossier `NewPhp8`) accessible dans le path (vérifiez dans une console avec `php -v`)
- MySQL ou MariaDb


## Première installation

- Dans une console, allez dans votre dossier www
- Cloner le projet avec la commande `git clone https://gitlab.com/csi-ort_etudiants/tutophp.git`

## Les dossiers

Les différentes démo sont organisées selon les dossiers qui correspondent à peu près aux parties de cours.

## Référence:

La doc de référence: https://www.php.net/manual/fr/

Version utilisable en local:
 
https://www.php.net/distributions/manual/php_manual_fr.tar.gz ou (Windows uniquement) https://www.php.net/distributions/manual/php_enhanced_fr.chm