<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/styles.css" />
    <title>Tuto FOR</title>
</head>
<body>
    <h1 class="titre">Itération FOR</h1>
    <div id="cadre_saisie" style="width:400px;">
	<p>
	<?php
		$no = 0;
		for($no=0; $no<10; $no++)
			echo "Faire un pas n° $no<br/>";
	?>
	</p>
	<p>Après la boucle, $no vaut <?= $no; ?></p>
    </div>
</body>
</html>
