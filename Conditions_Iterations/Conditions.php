<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Tuto PHP: les conditions</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <h1>Syntaxe des conditions en Php:</h1>
    <?php 
	    //  Utilisation de la fonction PHP mt_rand() pour générer un nombre aléatoire entre 0 et 100
	    $nbAleatoire = mt_rand(0, 100); 
    ?>
    <p>Le nombre généré est <?= $nbAleatoire; ?></p>
    <p>Les conditions peuvent s'écrire selon plusieurs syntaxes, mais produire le même résultat!</p>
    <h3>Syntaxe 1:</h3>
    <?php
	    if(($nbAleatoire % 2) == 0)
	    {
		    $res = "pair";			
	    }
	    else
	    {
		    $res = "impair";			
	    }
    ?>
    <p><?= $nbAleatoire ?> est <?= $res ?></p>			

    <h3>Syntaxe 2:</h3>
    <?php if(($nbAleatoire % 2) == 0) : ?>
	    <p><?= $nbAleatoire ?> est pair</p>
    <?php else : ?>
	    <p><?= $nbAleatoire ?> est impair</p>
    <?php endif ?>

    <h3>Syntaxe 3:</h3>
    <?php
	    $res = (($nbAleatoire % 2) == 0) ? "pair" : "impair";			
    ?>
	    <p><?= $nbAleatoire ?> est <?= $res ?></p>			
</body>
</html>