<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/styles.css" />
    <title>Tuto SWITCH</title>
</head>
    <?php
	//  Vérification que $_GET['meteo'] est une chaine de caractères "propre",
	//  la met en minuscule, au max 30 caractères,
	//  et l'affecte à la variable $meteo.
	$meteo = ($meteo = filter_input(INPUT_GET, 'meteo', FILTER_SANITIZE_SPECIAL_CHARS)) ? strtolower(substr($meteo, 0, 30)) : "";
    ?>
<body>
    <h1 class="titre">Choix multiple</h1>
    <div id="cadre_saisie" style="width:400px;">
        <!-- Le formulaire sera soumis à lui-même. Voir le principe de validation de l'attribut "action"... -->
	<form method="get" action="<?php echo filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL); ?>" id="formChoixMeteo">
	    <fieldset><legend>Météo:</legend>
	    <label for="meteo">Il fait: </label>
	    <!-- Le formulaire est soumis dès qu'une option est choisie (onchange="this.form.submit()). -->
	    <select name="meteo" id="meteo" onchange="this.form.submit()">
		    <option value="">Choisissez la météo</option>
		    <option value="beau">il fait beau</option>
		    <option value="pleut">il pleut</option>
		    <option value="neige">il neige</option>
		    <option value="vent">il fait du vent</option>
		    <option value="chaud">il fait chaud</option>
		    <option value="froid">il fait froid</option>
	    </select>
	    <br />
	    </fieldset>
	</form>
    </div>
    <div id="cadre_affichage">
	<p>
	<?php
	    switch($meteo){
		    case "chaud":
		    case "beau" : echo "Il fait bon, je vais à la piscine<br/>";
			    break;
		    case "pleut" : echo "Il pleut, je vais au cinéma<br/>";
			    break;	// Commenter de break pour voir son effet et la différence de comportement quand il pleut
		    case "neige" : echo "Il neige, je vais au ski<br/>";
			    break;
		    default : echo "Pas intéressant, je fais la sieste<br/>";
	    };
	?>
	</p>
    </div>
</body>
</html>
