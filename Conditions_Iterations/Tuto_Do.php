<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/styles.css" />
    <title>Tuto DO</title>
</head>
    <?php
	/**
	 * Simule le tirage aléatoire d'une carte.
	 * La carte tirée est "remise dans le paquet" et peut donc �tre tir�e une nouvelle fois...
	 * @return string la valeur (1 à Roi) et la couleur (Carreau, Pique, Coeur ou Trèfle) de la carte. Exemple: "7 Coeur"
	 */
	function tireUneCarte()
	{
	    $carte="";
	    $val = mt_rand(1, 13);
	    $couleur = mt_rand(0, 3);

	    switch($val)
	    {
		case 11: $carte = "Valet ";
			break;
		case 12: $carte = "Dame ";
			break;
		case 13: $carte = "Roi ";
			break;
		default : $carte = "$val ";
	    };

	    switch($couleur)
	    {
		case 0: $carte .= "Carreau";
			break;
		case 1: $carte .= "Pique";
			break;
		case 2: $carte .= "Coeur";
			break;
		case 3: $carte .= "Trèfle";
			break;
		default : $carte .= "Bizarre...";
	    };

	    return $carte;
	}
    ?>
<body>
    <h1 class="titre">Itération Do ... While</h1>
    <div id="cadre_saisie" style="width:400px;">
	<p>
	<?php
	    $carte = "";
	    $compteur = 0;
	    do
	    {
		$carte = tireUneCarte();
		echo "La carte est un $carte<br/>";
		$compteur++;
	    } while(($carte != "7 Carreau") && ($carte != "7 Coeur"))
	?>
	</p>
	<p>Après la boucle, $carte vaut <?= $carte ?></p>
	<p>$compteur vaut <?= $compteur; ?></p>
    </div>
</body>
</html>
