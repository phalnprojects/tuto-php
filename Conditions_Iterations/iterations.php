<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <title>Itérations en php</title>
</head>
<body>
    <h1>for, while, do ...</h1>
    <ul>
	<li>for($x=0; $x&lt;10; $x++)<br/>
    <?php
	    for($x=0; $x<10; $x++)
	    {
		    echo 'x='.$x.', ';
	    }
    ?>
	<br/>après l'itération, $x = <?php echo $x ?></li>
	<li>while($y&lt;10)<br/>
    <?php
	    $y=0;
	    while($y<10)
	    {
		    echo 'y='.$y++.', ';
	    }
    ?>
	<br/>après l'itération, $y = <?php echo $y ?></li>
	<li>do ... while($z&lt;10)<br/>
    <?php
	    $z=0;
	    do
	    {
		    echo 'z='.$z++.', ';
	    }
	    while($z<10);
    ?>
	<br/>après l'itération, $z = <?php echo $z; ?></li>
    </ul>

    <h1>Une autre écriture de for et while, pour un même résultat ...</h1>
    <ul>
	<li>for($x=0; $x&lt;10; $x++)<br/>
    <?php for($x=0; $x<10; $x++): ?>
	    x=<?= $x ?>, 
    <?php endfor ?>
	<br/>après l'itération, $x = <?= $x ?></li>
	<li>while($y&lt;10)<br/>
    <?php
	    $y=0;
	    while($y<10): ?>
		y=<?= $y++; ?>, 
    <?php endwhile ?>
	<br/>après l'itération, $y = <?= $y ?></li>
    </ul>
</body>
</html>