<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/styles.css" />
    <title>Tuto WHILE</title>
</head>
    <?php
	/**
	 * Simule la lecture d'une distance par rapport au panneau.
	 * La distance est générée aléatoirement entre 0 (donc au panneau...) et 25m.
	 * @return string Soit "panneau" si la distance est à 0m, soit "à $dist m" où dist est la distance du panneau
	 */
	function lirePosition()
	{
	    $msg = "";
	    $dist = mt_rand(0, 25);
	    if($dist == 0)
		$msg =  "panneau";
	    else
		$msg = "à $dist m";
	    return $msg;
	}
    ?>
<body>
    <h1 class="titre">Itération While</h1>
    <div id="cadre_saisie" style="width:400px;">
	<p>
	<?php
		$position = lirePosition();
		while($position != "panneau")
		{
			echo "Position=$position, donc bouge!<br/>";
			$position = lirePosition();
		}
	?>
	</p>
	<p>Après la boucle, $position vaut <?= $position ?></p>
    </div>
</body>
</html>
