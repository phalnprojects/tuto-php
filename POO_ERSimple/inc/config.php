<?php
// Url de base de l'application
define('URL_BASE', "http://localhost/TutosPhp/POO_ERSimple/");
// Définition des chemins:

define('BASE_DIR', dirname(dirname(__FILE__)));				    // Le dossier de l'application
// Le chemin, à partir de l'url de base, dans lequel chercher les classes.
define('CLASS_DIR', BASE_DIR.'/src/');

// Mise en place d'un autoload des classes, pour éviter les require / include des fichiers
// 1) le chemin des classe
set_include_path(get_include_path(). PATH_SEPARATOR . CLASS_DIR);
// 2) les extensions possibles pour les fichiers des classes
spl_autoload_extensions('.php,.class.php');
// 3) Enregistrement auprès de PHP des consognes de chargement des classes
spl_autoload_register();


//  Les informations de connexion à la BDD
require_once 'connect.php';


// Pour tracer les var_dump dans vos fichiers, définissez DUMP à TRUE
define('DUMP', FALSE);
if(DUMP)
{
    error_reporting(E_ALL);
    ini_set('display_errors','On');  
}
else
{
    ini_set('display_errors','Off');  
}

// Remplace la fonction var_dump si elle pose des problèmes d'affichage
// (c'est le cas si XDebug n'est pas installé)
function dump_var($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}