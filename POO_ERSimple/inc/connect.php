<?php
namespace Phaln;
class BDD
{
    static public $infoBdd = array(
	    'interface' => 'pdo',
	    'type'   => 'mysql',
	    'host'   => 'localhost',		
	    'dbname' => 'demoskel',		
	    'charset' => 'UTF8',
	    'user'   => 'root',			
	    'pass'   => '',		
	    'port'   => 3306
	);				
}