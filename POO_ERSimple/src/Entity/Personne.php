<?php
namespace Entity;

/**
 * Description d'une Personne, entité de la base de données.
 *
 * @author Philippe
 */
class Personne
{
    //  Les attributs, ils correspondent aux champs de la table
    protected ?int $id_pers = null;
    protected string $nom;
    protected string $prenom;

    /**
     * Constructeur
     * Il hydrate l'objet à partir du tableau $datas.
     * @param array $data les données d'hydratation
     */
    public function __construct(array $data = NULL)
    {
	$this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas=NULL) 
    {
	if(isset($datas['id_pers'])) $this->setid_pers ($datas['id_pers']);
	if(isset($datas['nom'])) $this->setnom ($datas['nom']);
	if(isset($datas['prenom'])) $this->setprenom ($datas['prenom']);
	return $this;
    }
	
    /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setId_pers($val=null) : self
    {
	if(is_null($this->id_pers))
	    $this->id_pers = ($tmp = filter_var ($val, FILTER_VALIDATE_INT)) ? $tmp : null;
	return$this;
    }
    
    /**
     * Les accesseurs (lecteurs et mutateurs) pour les attributs
     * 
     */
    function getId_pers() : ? int {
	return $this->id_pers;
    }

    function getNom() : ? string {
	return $this->nom;
    }

    public function setNom(string $nom) : self {
	$this->nom = $nom;
	return$this;
    }

    function getPrenom() : ? string {
	return $this->prenom;
    }

    public function setPrenom(string $prenom) : self {
	$this->prenom = $prenom;
	return$this;
    }
}