<?php
require_once '../inc/config.php';
error_reporting(E_ALL);
ini_set('display_errors','On');

use Entity\Personne;
use Repository\PersonneRepository;

$mapper = new PersonneRepository();
echo '<h1>fetchAll()</h1>';
$res = $mapper->getAll();
dump_var($res);

echo '<h1>getEntityId()</h1>';
echo '<h3>test 1</h3>';
$res = $mapper->getEntityId(11);
dump_var($res);
echo '<h3>test 2</h3>';
$res = $mapper->getEntityId(99);
dump_var($res);
echo '<h3>test 3</h3>';
$res = $mapper->getEntityId(-1);
dump_var($res);

echo '<h1>sauver()</h1>';
$datas = array(
	    'nom' => 'Play',
	    'prenom' => 'Henris',
);
$entity = new Personne($datas);
dump_var($entity);
echo '<h3>test 1</h3>';
$res = $mapper->sauver($entity);
dump_var($res);
$res->setnom('Modif');
$res->setid_pers(666);
dump_var($res);
echo '<h3>test 2</h3>';
$res = $mapper->sauver($res);
dump_var($res);

