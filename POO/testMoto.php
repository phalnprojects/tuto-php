<?php
//  Permet d'utiliser le typage fort si strict_types=1
//  !! Laisser en première ligne
declare(strict_types=1);

require_once 'config/appConfig.php';

echo'<h1>Instanciation Moto par défaut</h1>';
$ve = new Vehicules\Moto();
var_dump($ve);

echo'<h1>Instanciation Moto avec datas</h1>';
$ve2 = new Vehicules\Moto(array('carenage' => true, 'modele' => 'Desmosedici', 'immatriculation'=>'JU 999 RA'));
var_dump($ve2);

echo'<h1>Roule...</h1>';
echo '<p>'.$ve->roule().'</p>';
echo '<p>'.$ve2->roule().'</p>';