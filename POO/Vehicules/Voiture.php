<?php
namespace Vehicules;

/**
 * Description of Voiture
 *
 * @author phaln
 */
class Voiture extends VehiculeMotorise {
    protected $nbPortes;
    
    public function getNbPortes(): int {
	return $this->nbPortes;
    }

    public function __construct(?array $datas = null) {
	(!isset($datas['nbRoues'])) ? $datas['nbRoues'] = 4 : null;
	parent::__construct($datas);
	
	$this->nbPortes = (isset($datas['nbPortes'])) ? $datas['nbPortes'] : 2;
    }
   
    public function roule() {
	return 'Je suis une '.get_class($this).' à '.$this->nbPortes.' portes, '
		.(($this->getImmatriculation() == null)?'non ':null).'immatriculée ' .$this->getImmatriculation()
		.' et je penche plutôt à l\'extérieur du virage quand je tourne.';
    }

}
