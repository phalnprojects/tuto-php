<?php
namespace Vehicules;

/**
 * Description of Moto
 *
 * @author phaln
 */
class Moto extends VehiculeMotorise {
    protected $carenage;
    
    public function getCarenage() : bool {
	return $this->carenage;
    }
    
    protected function setCarenage(bool $carenage = false) : self {
	$this->carenage = $carenage;
	return $this;
    }

    public function __construct(?array $datas = null) {
	(!isset($datas['nbRoues'])) ? $datas['nbRoues'] = 2 : null;
	parent::__construct($datas);
	
	(isset($datas['carenage'])) ? $this->setCarenage($datas['carenage']) : $this->setCarenage();
    }
   
    public function roule() {
	return 'Je suis une '.get_class($this).(($this->getCarenage())?' avec ':' sans ').' carénage, '
		.(($this->getImmatriculation() == null)?'non ':null).'immatriculée '.$this->getImmatriculation()
		.' et je penche à l\'intérieur du virage quand je tourne.';
    }

}
