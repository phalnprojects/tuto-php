<?php
namespace Vehicules;

/**
 * Description of Vehicule
 *
 * @author phaln
 */
abstract class VehiculeMotorise {
    protected $modele;
    protected $nbRoues;
    private $immatriculation;
    
    public function getModele() : ?string {
	return $this->modele;
    }

    public function getNbRoues() : int {
	return $this->nbRoues;
    }

    public function getImmatriculation() : ?string {
	return $this->immatriculation;
    }

    public function setModele(?string $modele = null) : self {
	$this->modele = $modele;
	return $this;
    }

    public function setNbRoues(?int $nbRoues = 0) : self {
	$this->nbRoues = $nbRoues;
	return $this;
    }

    private function setImmatriculation(?string $immatriculation = null) : self {
	$this->immatriculation = $immatriculation;
	return $this;
    }

    public function __construct(? array $datas = null) {
	(isset($datas['modele'])) ? $this->setModele($datas['modele']) : $this->setModele();
	(isset($datas['nbRoues'])) ? $this->setNbRoues($datas['nbRoues']) : $this->setNbRoues();
	(isset($datas['immatriculation'])) ? $this->setImmatriculation($datas['immatriculation']) : $this->setImmatriculation();
    }

    abstract public function roule();
}
