<?php
declare(strict_types=1);

trait Counter {
    protected int $cptr = 0;
    
    public function increment() : static
    {
	$this->cptr++;
	return $this;
    }
    
    public function reset(?int $val=null) : static 
    {
	$this->cptr = (is_null($val)) ? 0 : $val;
	return $this;
    }
	    
}

trait ByTenCounter 
{
    protected int $cptr = 0;
    
    public function increment() : static
    {
	$this->cptr += 10;
	return $this;
    }
}

class Foo {
    //  Utilise le Trait Counter
    use Counter;
}

class Bar
{
    //  Utilkse les deux Traits Counter et ByTenCounter
    use Counter, ByTenCounter
    {
	// Lève l'mbiguité sur la méthode increment
	// Celle de ByTenCounter sera utilisée
	ByTenCounter::increment insteadof Counter;
    }
}
    
class Mother {
    //  Utilise le Trait Counter
    use Counter;
    
    //  Sa méthode locale surcharge celle du Trait
    public function increment() : static
    {
	$this->cptr += 2;
	return $this;
    }
    
}

class Baz extends Mother 
{
    //  Utilise le Trait ByTenCounter
    //  Il surcharge la méthode increment héritée de Mother
    use ByTenCounter;    
}
?>

<h2>Comptage avec Foo</h2>
<?php
$obj = new Foo();
var_dump($obj);
$obj->increment()->increment()->increment();
var_dump($obj);
$obj->reset();
var_dump($obj);
?>

<h2>Comptage avec Bar</h2>
<?php
$obj = new Bar();
var_dump($obj);
$obj->increment()->increment()->increment();
var_dump($obj);
$obj->reset();
var_dump($obj);
?>


<h2>Comptage avec Mother</h2>
<?php
$obj = new Mother();
var_dump($obj);
$obj->increment()->increment()->increment();
var_dump($obj);
$obj->reset();
var_dump($obj);
?>

<h2>Comptage avec Baz</h2>
<?php
$obj = new Baz();
var_dump($obj);
$obj->increment()->increment()->increment();
var_dump($obj);
$obj->reset();
var_dump($obj);
?>