<?php
class Tireur {
    private ? int $idTireur = null;
    private string $nom = 'vide';
    
    public function getIdTireur(): int {
	return $this->idTireur;
    }

    public function setIdTireur(? int $idTireur) {
	$this->idTireur = $idTireur;
	return $this;
    }

        public function getNom() : string {
	return $this->nom;
    }

    public function setNom(string $nom) : self {
	$this->nom = substr($nom, 0, 10);
	return $this;
    }
    
    public function __construct(array $datas = null) {
	(isset($datas['idTireur'])) ? $this->setIdTireur($datas['idTireur']) : $this->setIdTireur(null);
	if(isset($datas['nom']))
	    $this->setNom($datas['nom']);
	else
	    $this->setNom('Aucun');	
    }
}
