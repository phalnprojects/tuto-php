<?php
namespace AssoCompo;

/**
 * Description of Moteur
 *
 * @author phaln
 */
class Moteur {
    protected $puissance = 0.0;	    // en kW
    private $started = false;
    
    public function getPuissance() : float {
	return $this->puissance;
    }

    public function getStarted() : bool {
	return $this->started;
    }

    public function __construct(array $datas = null) {
	$tmp = 0.0;
	$this->puissance = ($tmp = filter_var($datas['puissance'], FILTER_VALIDATE_FLOAT) ) ? ($tmp >= 0.0)?$tmp : 0.0 : 0.0;
    }

    public function __toString() {
	$msg = get_class($this).';'.$this->getPuissance().';'.$this->getStarted();
	return $msg;
    }

    public function start() : void {
	$this->started = true;
	return;
    }

    public function stop() : void {
	$this->started = false;
	return;
    }
}
