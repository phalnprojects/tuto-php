<?php

namespace AssoCompo;

/**
 * Description of Vehicule
 *
 * @author phaln
 */
class Vehicule {

    protected $modele;
    private $immatriculation;
    
    private $lesRoues = [];
    protected $leMoteur = null;

    public function getModele(): ?string {
	return $this->modele;
    }

    public function getImmatriculation(): ?string {
	return $this->immatriculation;
    }

    public function getLesRoues(): array {
	return $this->lesRoues;
    }

    public function addRoue(Roue $roue): self {
	$this->lesRoues[] = $roue;
	return $this;
    }

    public function setModele(?string $modele = null): self {
	$this->modele = $modele;
	return $this;
    }

    private function setImmatriculation(?string $immatriculation = null): self {
	$this->immatriculation = $immatriculation;
	return $this;
    }

    public function __construct(?array $datas = null) {
	(isset($datas['modele'])) ? $this->setModele($datas['modele']) : $this->setModele();
	(isset($datas['immatriculation'])) ? $this->setImmatriculation($datas['immatriculation']) : $this->setImmatriculation();
	$this->leMoteur = (isset($datas['puissance'])) ? new Moteur($datas) : new Moteur(array('puissance' => 33.0929));
    }

    public function roule() {
	if (count($this->lesRoues) > 0) {
	    $this->leMoteur->start();
	    foreach ($this->lesRoues as $roue) {
		$roue->rotation(true);
	    }
	}
    }

    public function stoppe() {
	if (count($this->lesRoues) > 0) {
	    foreach ($this->lesRoues as $roue) {
		$roue->rotation(false);
	    }
	    $this->leMoteur->stop();
	}
    }

    public function __toString() {
	$msg = get_class($this) . ';' . $this->getModele() . ';' . $this->getImmatriculation();
	return $msg;
    }

}
