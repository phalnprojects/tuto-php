<?php
namespace AssoCompo;

/**
 * Description of Roue
 *
 * @author phaln
 */
class Roue {
    private $diametre;	// En pouce
    private $largeur;
    private $tourne = false;	
    
    public function getDiametre() : int {
	return $this->diametre;
    }

    public function getLargeur() : int {
	return $this->largeur;
    }

    public function getTourne() : bool {
	return $this->tourne;
    }
    
    public function __construct(?array $datas = null) {
	$this->diametre = ($tmp = filter_var($datas['diametre'], FILTER_VALIDATE_INT)) ? (int)$tmp : 8;
	$this->largeur = ($tmp = filter_var($datas['largeur'], FILTER_VALIDATE_INT)) ? (int)$tmp : 2;
    }

    public function rotation(?bool $tourne = false) : void
    {
	$this->tourne = $tourne;
	return;
    }
    
    public function __toString() {
	$msg = get_class($this).';'.$this->getLargeur().$this->getDiametre().';'.';'.(($this->getTourne())?'tourne':'fixe');
	return $msg;
    }

}
