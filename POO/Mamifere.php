<?php
/**
 * Description of Mamifere
 *
 * @author phaln
 */
class Mamifere {
    protected $terrestre;
    protected $nom = 'Inconnu';

    function getNom() {
	return $this->nom;
    }

    function setNom($nom) {
	$this->nom = $nom;
	return $this;
    }

        public function getTerrestre() {
	return $this->terrestre;
    }
    
    public function setTerrestre($terre) {
	$this->terrestre = $terre;
	return $this;
    }
    
    public function __construct(array $datas = null) {
	if(isset($datas['terrestre']))
	    $this->setTerrestre($datas['terrestre']);
	else
	    $this->setTerrestre(false);
	    
	(isset($datas['nom'])) ? $this->setNom($datas['nom']) : $this->setNom('Vide');
    }
}
