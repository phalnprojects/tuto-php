<?php
//  Permet d'utiliser le typage fort si strict_types=1
//  !! Laisser en première ligne
declare(strict_types=1);

require_once 'config/appConfig.php';

use AssoCompo\Vehicule;
use AssoCompo\Roue;

echo'<h1>Instanciation Vehicule par défaut</h1>';
$ve = new Vehicule();
var_dump($ve);

echo'<h1>Instanciation Voiture avec datas</h1>';
$ve2 = new Vehicule(array('modele' => 'Juke', 'immatriculation'=>'LY 666 ON', 'puissance'=>82.364));
var_dump($ve2);

echo'<h1>Création des 4 roues, puis ajout à $ve2</h1>';
$lesRoues = [];
for($i=0; $i<4; $i++) {
    $roue = new Roue(array('diametre'=>17, 'largeur'=>205));
    echo $roue.'<br/>';
    $lesRoues[] = $roue;
}
foreach ($lesRoues as $roue) {
    $ve2->addRoue($roue);
}
var_dump($ve2);


echo'<h1>Roule...</h1>';
echo'<p>Moteur en marche, les roues tournent</p>';
$ve2->roule();
var_dump($ve2);

echo'<p>Moteur à l\'arrêt, les roues fixes</p>';
$ve2->stoppe();
var_dump($ve2);
