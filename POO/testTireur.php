<?php
declare(strict_types=1);

require_once './Tireur.php';

$obj1 = new Tireur();
echo'<pre>';
var_dump($obj1);
echo'</pre><br/>';

$datas['idTireur'] = 66;
$datas['nom'] = 'Toto';
$obj2 = new Tireur($datas);
echo'<pre>';
var_dump($obj2);
echo'</pre><br/>';

$datas['idTireur'] = '99';
$datas['nom'] = 100;
$obj3 = new Tireur($datas);
echo'<pre>';
var_dump($obj3);
echo'</pre><br/>';

$datas['idTireur'] = 'toto';
$datas['nom'] = null;
$obj4 = new Tireur($datas);
echo'<pre>';
var_dump($obj4);
echo'</pre><br/>';
