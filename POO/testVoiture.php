<?php
//  Permet d'utiliser le typage fort si strict_types=1
//  !! Laisser en première ligne
declare(strict_types=1);

require_once 'config/appConfig.php';

echo'<h1>Instanciation Voiture par défaut</h1>';
$ve = new Vehicules\Voiture();
var_dump($ve);

echo'<h1>Instanciation Voiture avec datas</h1>';
$ve2 = new Vehicules\Voiture(array('nbPortes' => 5, 'modele' => 'Juke', 'nbRoues' => 4, 'immatriculation'=>'LY 666 ON'));
var_dump($ve2);

echo'<h1>Roule...</h1>';
echo '<p>'.$ve->roule().'</p>';
echo '<p>'.$ve2->roule().'</p>';