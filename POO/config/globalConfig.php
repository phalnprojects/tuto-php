<?php
// Définition des chemins:
define('BASE_DIR', dirname(__FILE__));				    // Le dossier de l'application


//  Définition du path d'inclusion
set_include_path(get_include_path() . PATH_SEPARATOR . BASE_DIR);


//  Autoload avec prise en compte des espaces de nom et compatibilité Linux (pb des séparateurs d'espace de nom...)
spl_autoload_register(function ($className) {
    $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    return spl_autoload($className);
});