<?php
//  Lancement des sessions
session_start();

//  Basculer à TRUE pour activer les affichages de debug, les var_dump ou les dump_var
define('DUMP', FALSE);

require_once 'globalConfig.php';
