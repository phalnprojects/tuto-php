<?php
function dump_var($var, $dump = DUMP, $msg = null) {
    if ($dump) {
	if ($msg) {
	    echo"<p><strong>$msg</strong></p>";
	}
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
    }
}

$str = "<p>Ceci est l'édition d'une note\r\nsur plusieurs <b>lignes</b> avec balises...</p>";
dump_var($str, true, 'Brute');
echo "$str";
echo '<br/>';
echo htmlentities($str);
echo '<br/>';

$fil = filter_var($str, FILTER_SANITIZE_STRING);
dump_var($fil, true, 'FILTER_SANITIZE_STRING !! Obsolète avec PHP 8.1');
echo "$fil";
echo '<br/>';
echo htmlentities($fil);
echo '<br/>';

$fil = filter_var($str, FILTER_SANITIZE_STRIPPED);
dump_var($fil, true, 'FILTER_SANITIZE_STRIPPED !! Obsolète avec PHP 8.1');
echo "$fil";
echo '<br/>';
echo htmlentities($fil);
echo '<br/>';

$fil = filter_var($str, FILTER_SANITIZE_SPECIAL_CHARS);
dump_var($fil, true, 'FILTER_SANITIZE_SPECIAL_CHARS');
echo "$fil";
echo '<br/>';
echo strip_tags($fil);
echo '<br/>';
echo htmlentities($fil);
echo '<br/>';

$fil = filter_var(strip_tags($str), FILTER_SANITIZE_SPECIAL_CHARS);
dump_var($fil, true, 'FILTER_SANITIZE_SPECIAL_CHARS avec fill striptaggé');
echo "$fil";
echo '<br/>';
echo strip_tags($fil);
echo '<br/>';
echo htmlentities($fil);
echo '<br/>';

$fil = filter_var($str, FILTER_SANITIZE_ADD_SLASHES );
dump_var($fil, true, 'FILTER_SANITIZE_ADD_SLASHES ');
echo "$fil";
echo '<br/>';
echo strip_tags($fil);
echo '<br/>';
echo htmlentities($fil);
echo '<br/>';

$fil = filter_var($str, FILTER_SANITIZE_FULL_SPECIAL_CHARS );
dump_var($fil, true, 'FILTER_SANITIZE_FULL_SPECIAL_CHARS ');
echo "$fil";
echo '<br/>';
echo strip_tags($fil);
echo '<br/>';
echo htmlentities($fil);
echo '<br/>';

$fil = filter_var($str, FILTER_UNSAFE_RAW );
dump_var($fil, true, 'FILTER_UNSAFE_RAW ');
echo "$fil";
echo '<br/>';
echo strip_tags($fil);
echo '<br/>';
echo htmlentities($fil);
echo '<br/>';

