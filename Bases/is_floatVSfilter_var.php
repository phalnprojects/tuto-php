<?php
echo'is_float';
var_dump(is_float(3.14));
var_dump(is_float(1));
var_dump(is_float(0));
var_dump(is_float(null));
var_dump(is_float('6.28'));

echo'filter_var';
var_dump(filter_var(3.14, FILTER_VALIDATE_FLOAT));
var_dump(filter_var(1, FILTER_VALIDATE_FLOAT));
var_dump(filter_var(0, FILTER_VALIDATE_FLOAT));
var_dump(filter_var(null, FILTER_VALIDATE_FLOAT));
var_dump(filter_var('6.28', FILTER_VALIDATE_FLOAT));
