<!DOCTYPE html>
<html>
    <head>
        <title>3-CSi Html/Css</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body>
        <h1>Index numérique</h1>
        <p>Un index est un nombre entier. Attention à la manière dont PHP interprête les nombres...</p>
        <?php
$t = array(
    1 => "a",
    "1" => "b",
    1.5 => "c",
    true => "d",
);
        var_dump($t);
        var_dump($t[1]);
        var_dump($t[0]);
        ?>

        <h1>Tableau associatifs</h1>
        <?php
        $couleursAs = array(
            'aqua' => 'rgb(0,  170,  200)',
            'black' => 'rgb(0,  0,  0)',
            'green' => 'rgb(0,  150,  100)',
        );
        var_dump($couleursAs);
        ?>		

        <h1>Tableau à trous</h1>
        <p>Les index d'un tableau ne sont pas obligés de se suivre, on peut avoir des trous et des index en désordre...</p>
        <p>L'ajout d'un élément avec les [] se fait à max index +1.</p>
        <?php
        $couleurs = ['cyan', 'magenta', 'jaune'];
        var_dump($couleurs);
        $couleurs[8] = null;
        $couleurs[] = "blanc";
        $couleurs[5] = "noir";
        $couleurs[7] = "rose";
        $couleurs[] = "";
        $couleurs[3] = "rouge";
        $couleurs[] = "vert";
        $couleurs[] = "gris";
        var_dump($couleurs);
        ?>
        <p>La suppression (!attention, "vider" une case n'est pas la supprimer...) avec 'unset()' </p>	
        <?php
        unset($couleurs[2]);
        unset($couleurs[7]);
        var_dump($couleurs);
        ?>

        <p>Le parcours d'un tableau à trous avec uns boucles for/while/do pose alors un problème...</p>
        <p>Exemple avec la boucle "while":</p>
        <ul>
            <?php
            $index = 0;
            while ($couleurs[$index]) {
                echo '<li>' . $couleurs[$index] . '</li>';
                $index += 1;
            }
            ?>
        </ul>

        <p>Autre exemple avec la boucle "for":</p>
        <ul>
            <?php
            $index = 0;
            for ($index = 0; $index < count($couleurs); $index++) {
                echo '<li>' . $couleurs[$index] . '</li>';
            }
            ?>
        </ul>

        <p>Prévilégiez la boucle "foreach()"</p>
        <ul>
            <?php
            foreach ($couleurs as $coul) {
                echo '<li>' . $coul . '</li>';
            }
            ?>
        </ul>

        <p>Vous pouvez le trier simplement, en respectant l'ordre des clés/index:</p>
        <?php
        ksort($couleurs);
        var_dump($couleurs);
        ?>

        <p>Présenté dans une table HTML, c'est mieux...</p>
        <table>
            <thead>
                <tr><th>Index</th><th>Valeur</th></tr>
            </thead>
            <?php
            foreach ($couleurs as $key => $value) :
                echo "<tr><td>$key</td><td>$value</td></tr>";
            endforeach;
            ?>
        </table>

        <h1>Tableau associatif</h1>
        <p>Les index numériques sont remplacés par une clé (chaîne de caractères).</p>
        <?php
        //  Tableau vide
        $personne = array();

        //  Ajout des éléments
        $personne['prenom'] = 'René';
        $personne['nom'] = 'Sence';
        $personne['age'] = 23;
        var_dump($personne);
        ?>
        <p>Les valeurs.</p>
        <?php
        foreach ($personne as $val) {
            echo "$val ";
        }
        ?>
        <p>Les clés ET les valeurs.</p>
        <?php
        foreach ($personne as $key => $val) {
            echo "$key = $val<br/>";
        }
        ?>

        <h1>Tableau multi-dimentsions</h1>
        <p>On peut faire des tableaux de tableaux...</p>
        <?php
        $lesPersonnes = [
            [
                'prenom' => 'Betty',
                'nom' => 'Monde',
                'age' => 25,
            ],
            [
                'prenom' => 'Jim',
                'nom' => 'El-Paké',
                'age' => 22,
            ],
        ];
        var_dump($lesPersonnes);
        $lesPersonnes[] = $personne;
        var_dump($lesPersonnes);
        ?>

        <h1>Tableau PHP vers Table HTML</h1>
        <p>L'affichage d'un tableau PHP dans une table HTML est une demande récurente.</p>
        <table>
            <thead>
                <tr><th>Prénom</th><th>Nom</th><th>Age</th></tr>
            </thead>
            <tbody>
                <?php foreach ($lesPersonnes as $unePers): ?>
                    <tr>
                        <td><?= $unePers['prenom'] ?></td>
                        <td><?= $unePers['nom'] ?></td>
                        <td><?= $unePers['age'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p>On peut aussi le faire avec 2 itérations imbriquées plus une 3ème pour récupérer les clés.</p>
        <?php
        $keys = array_keys($lesPersonnes[array_key_first($lesPersonnes)]);
        ?>
        <table>
            <thead>
                <tr>
                    <?php foreach ($keys as $key): ?>
                        <th><?= ucfirst($key) ?></th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($lesPersonnes as $unePers): ?>
                    <tr>
                        <?php foreach ($unePers as $val): ?>
                            <td><?= $val ?></td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h1>Tableau "fourre tout"</h1>
        <p>Un tableau peut mélanger index et clés...</p>
        <?php
        $t = array(
            1 => "a",
            "b",
            40 => 5,
            3.14,
            "multi" => array(
                "dim" => [
                    "array" => "foo"
                ],
            ),
            'multi2' => 666,
            12 => '12',
        );
        $t[] = 13;
        $t[39] = 'Trente neuf';
        $t[] = 'Combien?';
        var_dump($t);
        ?>
        <h1>Compact</h1>
        <p>Rassembler des variables dans un tableau...</p>
        <?php
$var1 = 10;
$uneAutre = 'Machin';
$derniere = 3.14;
$tab = [11, 22, 'der' => 'une String'];
$var = compact('var1', 'tab', 'uneAutre', 'derniere');
        var_dump($var);
        ?>
        <h1>Extract</h1>
        <p>Décompresser des variables depuis un tableau...</p>
        <?php
        unset($var1);
        unset($uneAutre);
        unset($derniere);
        unset($tab);
extract($var);
var_dump($var1);
var_dump($uneAutre);
var_dump($derniere);
var_dump($tab);
        ?>
    </body>
</html>
