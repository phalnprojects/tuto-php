<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Typage fort</title>
	<link rel="stylesheet" href="../css/normalize.css">
	<link rel="stylesheet" href="../css/styles.css">
    </head>
    <body>
        <?php
        function incremente($nombre)
        {
            $nombre += 1;
            return $nombre;
        }
        
        function incrementeType(int $nombre) : int
        {
            $nombre += 1;
            return $nombre;
        }
        
        function returnVoid(int $nombre) : void
        {
            $nombre += 1;
	    var_dump($nombre);
            return;
        }
	
        $nombreInt = 10;
        $nombreString = "10";
        $res = 0;
        ?>
        <h1>Utilisation sans type</h1>
        <p>incremente($nombreInt) = <?= $res=incremente($nombreInt) ?></p>
	<?php var_dump($res); ?>
        <p>incremente($nombreString) = <?= $res=incremente($nombreString) ?></p>
	<?php var_dump($res); ?>
        <h1>Utilisation avec type</h1>
        <p>le typage fort nécessite l'usage des exceptions...</p>
        <?php
            try 
            {
                $res=incrementeType($nombreInt);
		var_dump($res); 
                echo "<p>incrementeType($nombreInt) = $res</p>";
//                $res=incrementeType($nombreString);
//		var_dump($res); 
//                echo "<p>incrementeType($nombreString) = $res</p>";
                $res = returnVoid($nombreInt);
		var_dump($res);
                echo "<p>returnVoid($nombreInt) = $res</p>";
            } catch (TypeError $ex) 
            {
                echo "<p>incrementeType ne s'utilise pas avec des paramètres de type String...</p>";
            }
        ?>
    </body>
</html>
