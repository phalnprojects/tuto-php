<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Tuto PHP</title>
    <meta charset="utf-8">
</head>

<body>
    <?php
	$message = 'en PHP';	//  Une variable contenant une chaîne de caractères

	//  Démonstration de la différence entre les " et les '
	echo '<p style="color:blue;">Ce texte est écrit ' . $message .' avec des simples quotes</p>';
	echo "<p style=\"color:red;\">Ce texte est écrit $message avec des doubles quotes</p>";
    ?>
    <p>Celui-ci est aussi <?= $message; ?>, enfin juste les deux mots "en PHP" sur cette ligne!</p>
</body>
</html>