<head>
    <title>Démo HTML / CSS</title>
    <meta charset="utf-8">
    <!--    Vos feuilles de style ici.
	    Commencez par normalize.css qui fait le "reset" css.
	    Personnalisez main.css ou ajoutez d'autres fichiers.
    -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
</head>