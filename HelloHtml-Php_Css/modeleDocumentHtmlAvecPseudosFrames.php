<?php
    declare(strict_types=1);
?>
<!DOCTYPE html>
<html lang="fr">
<?php
    include_once('inc/head.php');
?>
<body>
<?php
    include_once('inc/header.php');
?>
    <div id="main">
	<!-- La partie principale de votre page ici -->
	<nav>
            <?php
                include_once('inc/menu.php');
            ?>
	</nav>
	<div id="corps">
    	<!-- Le contenu de votre page ici -->
	    <h1>Hello !!</h1>
	    <p>Cette page a été vue <strong>191</strong> fois.</p>
	</div>
    </div>
<?php
    include_once('inc/footer.php');
?>
</body>
</html>