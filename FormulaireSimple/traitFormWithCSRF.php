<?php
session_start();

define('DUMP', false);

$error = true;
//  On récupère le verbe HTTP qui nous ammène ici (GET ou POST
$request = strtoupper(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS));

switch ($request) {
    case'POST':
	//  Cette partie de code sera exécuté seulement lors d'une requète POST, c'est à  dire lorsque le formulaire sera soumis.
	//  Récupération du token dansle formulaire
	$token = ($tmp = filter_input(INPUT_POST, 'csrftoken', FILTER_SANITIZE_SPECIAL_CHARS)) ? $tmp : '';
	if (DUMP) {
	    echo"token<br/>";
	    var_dump($token);
	}
	if (!empty($_SESSION['token']) && $token !== '' && hash_equals($_SESSION['token'], $token)) {
	    //  Le token est correct
	    //  hash_equals — Comparaison de chaînes résistante aux attaques temporelles

	    $error = false;
	    // Récupération sécurisée des champs du formulaire
	    // On en profite pour mettre en majuscule si nécessaire
	    // et pour vérifier la longueur (pour utilisation avec bdd, identique à la longueur du champ dans la table...)
	    $nom = ($tmp = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS)) ? substr(strtoupper(trim($tmp)), 0, 30) : '';
	    $prenom = ($tmp = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS)) ? substr(trim($tmp), 0, 30) : '';
	} else {
	    //  Erreur sur le token
	    $error = true;
	}
	break;
    default:
	// On est arrivé ici avec autre chose que POST...
	$url = 'http://localhost/dev/TutosPhp/FormulaireSimple/formWithCSRF.php';
	header("location: $url");
	break;
}
if (DUMP) {
    echo"request: ";
    var_dump($request);
    echo"<br/>SESSION['token']>: ";
    var_dump($_SESSION['token']);
    echo"<br/error: ";
    var_dump($error);
}
?>
<!DOCTYPE html>
<html>
    <head>
	<title>Html/Css</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
    </head>
    <body>
	<div id="cadre_affichage">
	    <?php
	    if ('POST' === $request && !$error):
		//  Cette partie de code sera exécuté seulement lors d'une requète POST, c'est à  dire lorsque le formulaire sera soumis.
		?>
                <h1>Votre réponse:</h1>	
                <p>Bonjour <?= $prenom ?> <?= $nom ?>!</p>
                <p>Vous avez complété le formulaire et utilisé le bouton "Enregistrer"...</p>
	    <?php else: ?>
                <!-- Cette partie ne s'affiche que lors d'une requète GET -->
                <a href="http://localhost/dev/TutosPhp/FormulaireSimple/formWithCSRF.php">Complétez CE formulaire...</a>
	    <?php endif; ?>
	</div>
    </body>
</html>
