<!DOCTYPE html>
<html>
<head>
    <title>Html/Css</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <h1>Formulaire ...</h1>
    <p>Le formulaire est soumis à  lui-même (voir action=...) en POST.</p>
    <p>On a donc 2 comportements différents selon le vrebe http d'accès:</p>
    <ul>
	<li>En GET (via l'url...) le formulaire s'affiche avec le message "Complétez le formulaire.".</li>
	<li>En POST (après avoir complété le formulaire et utiliser le bouton "Enregistrer"...) le formulaire s'affiche avec, en dessous, les informations saisies.</li>
    </ul>
    <div id="cadre_saisie">
	<!-- Le formulaire est soumis à  lui-même (voir action=...) en POST.  -->
	<form method="post" action="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL); ?>">
	    <fieldset><legend>Votre identité:</legend>
		<label for="prenom">Prénom: </label>
		    <input id="prenom" type="text" name="prenom" maxlength="30" size="30" />
		<label for="nom">Nom: </label>
		    <input id="nom" type="text" name="nom" required="required" maxlength="30" size="30" />
		<br/>
		<input type="submit" value="Enregistrer" />
	    </fieldset>
	</form>
    </div>
    <div id="cadre_affichage">
    <?php
	//  Cette partie de code sera exécuté seulement lors d'une requète POST, c'est à  dire lorsque le formulaire sera soumis.
	if(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS) === 'POST'):
	    // Récupération sécurisée des champs du formulaire
	    // On en profite pour mettre en majuscule si nécessaire
	    // et pour vérifier la longueur (pour utilisation avec bdd, identique à la longueur du champ dans la table...)
	    $nom = ($nom = strtoupper(filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS))) ? substr(strtoupper($nom), 0, 30) : "";
	    $prenom = ($prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS))? substr($prenom, 0, 30) : "";
    ?>
	<h1>Votre réponse:</h1>
	<p>Bonjour <?= $prenom ?> <?= $nom ?>!</p>
	<p>Vous avez complété le formulaire et utilisé le bouton "Enregistrer"...</p>
    <?php else:	?>
	<!-- Cette partie ne s'affiche que lors d'une requète GET -->
	<p>Complétez le formulaire.</p>
    <?php endif; ?>
    </div>
</body>
</html>
