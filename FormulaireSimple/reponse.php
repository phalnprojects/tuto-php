<!DOCTYPE html>
<html>
<head>
    <title>Html/Css</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <h1>Votre réponse au formulaire:</h1>
    <?php
	$nom = $_POST['nom'];
	//  Ceci est à ne JAMAIS faire!!!
	//  Prenez soin de TOUJOURS filtrer vos entrées de données (ici INPUT_POST)
	//  avec le filtre adéquat et de donner des valeurs par défaut.
	//  Cela vous évitera certaines failles de sécurité.
	$nom = ($nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS)) ? $nom : 'Pas de nom';
	//  La ligne ci-dessus utilise l'opérateur ternaire "?". Elle pourrait aussi s'écrire:
	//	$nom = filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_STRING);
	//	if($nom==NUL || $nom==FALSE)
	//	    $nom = 'Pas de nom';
	
	$niveau = ($niveau = filter_input(INPUT_POST, 'niveau', FILTER_SANITIZE_SPECIAL_CHARS)) ? $niveau : 'Pas de niveau';
    ?>
    <!-- Si vous avez utilisé un filtre, pas besoin d'échapper les variable (avec htmlentities ou équivalent). -->    
    <p>Votre nom: <?= $nom ?> et votre niveau: <?= $niveau ?></p>
</body>
</html>
