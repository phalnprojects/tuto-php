<?php
session_start();

define('DUMP', false);

// On génère le token.
// random_bytes — Génère des octets pseudo-aléatoire cryptographiquement sécurisé
// bin2hex — Convertit des données binaires en représentation hexadécimale
$_SESSION['token'] = bin2hex(random_bytes(32));

if (DUMP) {
    echo"<br/>SESSION['token']>: ";
    var_dump($_SESSION['token']);
}
?>
<!DOCTYPE html>
<html>
    <head>
	<title>Html/Css</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
    </head>
    <body>
	<h1>Formulaire ...</h1>
	<div id="cadre_saisie">
	    <form method="post" action="traitFormWithCSRF.php">
		<input type="hidden" name="csrftoken" value="<?= (isset($_SESSION['token'])) ? $_SESSION['token'] : '' ?>"/>
		<fieldset><legend>Votre identité:</legend>
		    <label for="prenom">Prénom: </label>
		    <input id="prenom" type="text" name="prenom" maxlength="30" size="30" />
		    <label for="nom">Nom: </label>
		    <input id="nom" type="text" name="nom" required="required" maxlength="30" size="30" />
		    <br/>
		    <input type="submit" value="Enregistrer" />
		</fieldset>
	    </form>
	</div>
    </body>
</html>
