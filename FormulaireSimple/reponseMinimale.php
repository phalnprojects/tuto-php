<!DOCTYPE html>
<html>
<head>
    <title>Html/Css</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <h1>Votre réponse au formulaire:</h1>
    <?php
    var_dump($_GET);
    
	//  Normalement, ceci est à ne JAMAIS faire!!!
	//  Pour des raisons de sécurité, on utilise jamais directement $_GET ou $_POST
    
	//  Récupération des données du formulaire dans le tableau correspondant à method dans le formulaire.
	$nom = $_GET['toto'];
	$niveau = $_GET['niveau'];
    ?>
    <p>Sans sécurité<br/>Votre nom: <?= $nom ?> et votre niveau: <?= $niveau ?></p>
    
    <!-- Si vous n'avez pas utilisé un filtre, il faut "échapper" les variable (avec htmlentities ou équivalent). -->    
    <p>Mieux...<br/>Votre nom: <?= htmlentities($nom) ?> et votre niveau: <?= htmlentities($niveau) ?></p>
</body>
</html>
