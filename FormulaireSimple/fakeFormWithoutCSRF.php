<!DOCTYPE html>
<html>
    <head>
	<title>Html/Css</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
    </head>
    <body>
	<h1>Fake Formulaire ...</h1>
	<div id="cadre_saisie">
	    <form method="post" action="traitFormWithCSRF.php">
		<input type="hidden" name="csrftoken" value="f97cc7a2443c773776d6aa52d6ac1d16d28f770e1209a576e5cb505296e3e019"/>
		<fieldset><legend>Votre identité:</legend>
		    <label for="prenom">Prénom: </label>
		    <input id="prenom" type="text" name="prenom" />
		    <label for="nom">Nom: </label>
		    <input id="nom" type="text" name="nom" />
		    <br/>
		    <input type="submit" value="Enregistrer" />
		</fieldset>
	    </form>
	</div>
    </body>
</html>
