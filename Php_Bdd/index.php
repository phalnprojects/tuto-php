<!DOCTYPE html>
<html lang="fr">
    <head>
    <meta charset="utf-8">
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/main.css">
	<link rel="stylesheet" href="./css/styles.css">
	<title>BDD en Php</title>
    </head>
    <body>
	<h1 class="titre">Menu:</h1>	
	<div id="cadre_saisie">
	    <ul>
		<li>MySQLi en procédural:</li>
		<ul>
		    <li><a href="./mySqli_proc/form_mysqli_proc.html">Nouvelle personne</a></li>
		    <li><a href="./mySqli_proc/index.php">Liste des personnes</a></li>
		    <li><a href="./mySqli_proc/form_mysqli_proc_prep.php">Requêtes préparées</a></li>
		</ul>
		<li>MySQLi objet:</li>
		<ul>
		    <li><a href="./bddObjet/form_mysqli_obj.php">MySQLi objet</a></li>
		    <li><a href="./bddObjet/form_mysqli_obj_prep.php">MySQLi objet, requêtes préparées</a></li>
		</ul>
		<li>PDO:</li>
		<ul>
		    <li><a href="./bddObjet/liste_pdo.php">Select en PDO</a></li>
		    <li><a href="./bddObjet/form_pdo.php">Insertion en PDO</a></li>
		    <li><a href="./bddObjet/form_pdo_prep.php">PDO, requêtes préparées</a></li>
		</ul>
	    </ul>
	</div>
    </body>
</html>
