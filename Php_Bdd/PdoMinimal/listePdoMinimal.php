<!DOCTYPE html>
<html lang="fr">
    <head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../css/normalize.css">
	<link rel="stylesheet" href="../css/main.css">
	<title>Démo PDO minimale</title>
    </head>
    <body>
	<h1 class="titre">BdD avec PDO ...</h1>
	<?php
	//  Les données nécéssaires à la connexion
	$hostName = 'localhost';    // URL serveur
	$dbName = 'test_gestmedia';	    // nom de la base de données
	$userName = 'root';	    // nom d'utilisateur 
	$userPassword = '';    // mot de passe
	$charset = 'utf8';	    // l'encodage des caractères dans la bdd
	
	//  Connexion à la BDD
	$dsn = "mysql:host=$hostName;dbname=$dbName;charset=$charset";
	$connexion = new PDO($dsn, $userName, $userPassword);

	//  Exécution de la requête SQL
	$selectPersQuery = "SELECT * FROM personne ORDER BY nom";
	$resultats = $connexion->query($selectPersQuery);

	//  Affichage des résultats dans une table HTML
	?>
	<table>
	    <!--  La ligne de titre  -->
	    <tr><th>Id</th><th>Prénom</th><th>Nom</th></tr>
	    <?php
	    //  On détermine de quelle manière on veut parcourir les résultats:
	    //  ici sous forme de tableau associatif.
	    $resultats->setFetchMode(PDO::FETCH_ASSOC);

	    //  Les résultats sont comme un tableau scalaire.
	    //  Chaque ligne est un tableau associatif dont les clès sont
	    //  les noms des champs de la bdd récupérés dans la requète SQL.
	    foreach ($resultats as $unePers): //  Parcour la collection de résultats 
		?>
    	    <!--  Chaque ligne des extraite des résultats ($unePers) sera dans une ligne de la table HTML  -->
    	    <tr><td><?= $unePers['id_pers'] ?></td><td><?= $unePers['prenom'] ?></td><td><?= strtoupper($unePers['nom']) ?></td></tr>
	    <?php endforeach; ?>
	</table>
    </body>
</html>
