<!DOCTYPE html>
<html lang="fr">
    <head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../css/normalize.css">
	<link rel="stylesheet" href="../css/main.css">
	<title>Démo PDO</title>
    </head>
    <body>
	<h1 class="titre">BdD avec PDO ...</h1>	
	<form method="post" action="traitPdoMinimal.php">
	    <fieldset><legend>Votre identité:</legend>
		<label for="prenom">Prénom: </label>
		<input id="prenom" type="text" name="prenom" required="required" maxlength="30" /><br/>
		<label for="nom">Nom: </label>
		<input id="nom" type="text" name="nom" required="required" maxlength="30" /><br/>
		<input type="submit" value="Enregistrer" />
	    </fieldset>
	</form>
    </body>
</html>
