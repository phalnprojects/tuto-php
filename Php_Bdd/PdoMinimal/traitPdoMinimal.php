<?php
//var_dump($_POST);

//  On s'assure qu'on arrive bien selon la méthode POST
if ('POST' === $_SERVER['REQUEST_METHOD']) 
{
    //  Les données nécéssaires à la connexion
    $hostName = 'localhost';    // URL serveur
    $dbName = 'test_gestmedia';	    // nom de la base de données
    $userName = 'root';	    // nom d'utilisateur 
    $userPassword = '';    // mot de passe
    $charset = 'utf8';	    // l'encodage des caractères dans la bdd

    //  Connexion à la BDD
    $dsn = "mysql:host=$hostName;dbname=$dbName;charset=$charset";
    $connexion = new PDO($dsn, $userName, $userPassword);

    //  ATTENTION! Ne pas oublier d'échapper les chaines composant une requête...
    //  lorsqu'elles proviennent d'un formulaire et que vous n'utilisez pas filter_input(...)  
    $nom = strtoupper($connexion->quote($_POST['nom']));
    $prenom = $connexion->quote($_POST['prenom']);

    //  La requète SQL d'insertion
    $insertPersQuery = "INSERT INTO personne(nom,prenom) VALUES ($nom, $prenom);";
    //var_dump($insertPersQuery);
    
    $count = $connexion->exec($insertPersQuery);
    //var_dump($count);
}

//  Redirection vers la liste des personnes
header("location: listePdoMinimal.php");
