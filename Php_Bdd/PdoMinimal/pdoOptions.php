<?php
declare(strict_types=1);

define('DUMP', true);
define('PERSONNE_FILE', '../../POO_ERSimple/src/Entity/Personne.php');

require_once(PERSONNE_FILE);

function dump_var($var, $dump = DUMP, $msg = null) {
    if ($dump) {
	if ($msg) {
	    echo"<p><strong>$msg</strong></p>";
	}
	if (function_exists('dump'))
	    dump($var);
	else {
	    echo '<pre>';
	    var_dump($var);
	    echo '</pre>';
	}
    }
}

//  Les données nécéssaires à la connexion
$hostName = 'localhost';    // URL serveur
$dbName = 'test_gestmedia';     // nom de la base de données
$userName = 'root';     // nom d'utilisateur 
$userPassword = '';    // mot de passe
$charset = 'utf8';     // l'encodage des caractères dans la bdd

//  Connexion à la BDD
$dsn = "mysql:host=$hostName;dbname=$dbName;charset=$charset";
$connexion = new PDO($dsn, $userName, $userPassword);

//  La requête SQL
$selectPersQuery = "SELECT * FROM personne ORDER BY nom";

echo '<h1>Avec émulation (comportement par défaut)</h1>';
echo '<h2>Reqête classique</h2>';
$resultat = $connexion->query($selectPersQuery);
$var = $resultat->fetch(PDO::FETCH_ASSOC);
dump_var($var, DUMP, 'Fetch FETCH_ASSOC:');

echo '<h2>Reqête préparée</h2>';
$reqPrep = $connexion->prepare($selectPersQuery);

//  Exécution de la requête SQL
$resultats = $reqPrep->execute();
dump_var($resultats, DUMP, 'Exécution rqt:');

$var = $reqPrep->fetch();
dump_var($var, DUMP, 'Fetch BOTH par défaut:');

$var = $reqPrep->fetch(PDO::FETCH_OBJ);
dump_var($var, DUMP, 'Fetch FETCH_OBJ:');

$reqPrep->setFetchMode(PDO::FETCH_ASSOC);
$var = $reqPrep->fetch();
dump_var($var, DUMP, 'Fetch FETCH_ASSOC:');

$reqPrep->setFetchMode(PDO::FETCH_CLASS, 'Entity\Personne');
$var = $reqPrep->fetchAll();
dump_var($var, DUMP, 'Fetch FETCH_CLASS Personne:');

echo '<h1>Avec option EMULATE à false</h1>';
//  Désactive la simulation des requêtes préparées pour utiliser l'interface native
//  Permet de conserver les types MySQL lors d'un Fetch.
$connexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

echo '<h2>Reqête classique</h2>';
$resultat = $connexion->query($selectPersQuery);
$var = $resultat->fetch(PDO::FETCH_ASSOC);
dump_var($var, DUMP, 'Fetch FETCH_ASSOC:');

echo '<h2>Reqête préparée</h2>';
$reqPrep = $connexion->prepare($selectPersQuery);

//  Exécution de la requête SQL
$resultats = $reqPrep->execute();
dump_var($resultats, DUMP, 'Exécution rqt:');

$reqPrep->setFetchMode(PDO::FETCH_BOTH);
$var = $reqPrep->fetch();
dump_var($var, DUMP, 'Fetch BOTH par défaut:');

$var = $reqPrep->fetch(PDO::FETCH_OBJ);
dump_var($var, DUMP, 'Fetch FETCH_OBJ:');

$reqPrep->setFetchMode(PDO::FETCH_ASSOC);
$var = $reqPrep->fetch();
dump_var($var, DUMP, 'Fetch FETCH_ASSOC:');

$reqPrep->setFetchMode(PDO::FETCH_CLASS, 'Entity\Personne');
$var = $reqPrep->fetchAll();
dump_var($var, DUMP, 'Fetch FETCH_CLASS Personne:');
