<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>Démo MySQLi</title>
</head>
<?php
    include_once 'connect_mysqli.inc.php';

    $connexion = connectMySQLi();

    $sqlInsertPersonne = "INSERT INTO personne(nom,prenom) VALUES (?, ?)";
    $reqInsPrep=$connexion->prepare($sqlInsertPersonne);
    $sqlSelectPersonne = "SELECT * FROM personne ORDER BY nom";
    $reqSelect = $connexion->prepare($sqlSelectPersonne);
    if($reqInsPrep === FALSE || $reqSelect === FALSE)
	header("location: dbError.html");		    

    if(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS) == 'POST')
    {
	$nom = ($nom = strtoupper(filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS))) ? substr(strtoupper($nom), 0, 30) : "";
	$prenom = ($prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS))? substr($prenom, 0, 30) : "";

	if($nom != "" && $prenom != "")
	{
	    $reqInsPrep->bind_param('ss', $nom, $prenom);
	    if($reqInsPrep->execute() === FALSE)
	    {
		header("location: ../error/dbError.html");
	    }
	}
    }
?>
<body>
    <h1 class="titre">BdD avec les requêtes préparées de MySQLi ...</h1>	
    <div id="cadre_saisie">
	<form method="post" action="<?php echo (filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL)); ?>" id="formulaire">
	    <fieldset><legend>Votre identité:</legend>
		<label for="prenom">Prénom: </label><input id="nom" type="text" name="prenom" required="required" maxlength="30" size="30" />
		<label for="nom">Nom: </label><input id="prenom" type="text" name="nom" required="required" maxlength="30" size="30" />
		<br/>
		<input type="submit" value="Enregistrer" />
	    </fieldset>
	</form>
    </div>
    <div id="cadre_affichage">
    <?php
	if($reqSelect->execute() === FALSE)
	{
	    header("location: ../error/dbError.html");
	}

	?>
	<h2>Tableau associatif</h2>
	<table summary="Liste des personnes">
	<?php
	$resultats = $reqSelect->get_result();
	while($unePers = $resultats->fetch_assoc()):
	?>

	<tr>
	    <td><?= $unePers["id_pers"]; ?></td>
	    <td><?= $unePers["prenom"]; ?></td>
	    <td><?= strtoupper($unePers["nom"]); ?></td>
	</tr>
	<?php endwhile; ?>
	</table>

	<?php
	    $resultats->close(); // ferme les résultats
	    $connexion->close();
       ?>
    </div>
</body>
</html>
