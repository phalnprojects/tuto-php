<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>Démo PDO</title>
</head>
<?php
    require_once '../connect.inc.php';        
?>
<body>
    <h1 class="titre">BdD avec PDO ...</h1>
    <div>
    <?php
	try
	{
	    //  Connexion à la BDD
	    $dsn = 'mysql:host='.$hostName.';dbname='.$dbName;
	    $connexion = new PDO($dsn, $userName, $userPassword,
		array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	    
	    //  Exécution de la requête SQL
	    $selectPersQuery = "SELECT * FROM personne ORDER BY nom";
	    $resultats=$connexion->query($selectPersQuery);

	    //  Affichage des résultats
	    ?>
	    <table style="width: 80%; color: lightgrey" summary="Liste des personnes">
		<tr><th>Id</th><th>Prénom</th><th>Nom</th></tr>
		<?php while($unePers = $resultats->fetch(PDO::FETCH_ASSOC)): //  Parcour la collection de résultats ?>
		    <tr><td><?= $unePers['id_pers'] ?></td><td><?= $unePers['prenom'] ?></td><td><?= strtoupper($unePers['nom']) ?></td></tr>
		<?php endwhile; ?>
	    </table>
	<h1 class="titre">Deuxième forme de parcours des résultats</h1>
	    <table style="width: 80%; color: lightgrey" summary="Liste des personnes">
		<tr><th>Id</th><th>Prénom</th><th>Nom</th></tr>
		<?php 
		    $resultats=$connexion->query($selectPersQuery);
		    
		    $resultats->setFetchMode(PDO::FETCH_ASSOC);
		    
		    foreach($resultats as $unePers): //  Parcour la collection de résultats ?>
			<tr><td><?= $unePers['id_pers'] ?></td><td><?= $unePers['prenom'] ?></td><td><?= strtoupper($unePers['nom']) ?></td></tr>
		<?php endforeach; ?>
	    </table>
	
	<?php 
	    $resultats->closeCursor(); // ferme les résultats
	}
	catch(Exception $e)
	{
	    header("location: ../error/dbError.html") ;
	}
    ?>
    </div>
</body>
</html>
