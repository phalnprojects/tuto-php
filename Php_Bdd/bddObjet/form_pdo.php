<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>Démo PDO</title>
</head>
<?php
    require_once '../connect.inc.php';
        
    if('POST' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS))
    {
	$nom = ($nom = strtoupper(filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS))) ? substr(strtoupper($nom), 0, 30) : "";
	$prenom = ($prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS))? substr($prenom, 0, 30) : "";

	if($nom != "" && $prenom != "")
	{
	    try
	    {
		$dsn = 'mysql:host='.$hostName.';dbname='.$dbName;
		$connexion = new PDO($dsn, $userName, $userPassword,
		array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		//  ATTENTION! Ne pas oublier d'échapper les chaines composant une requête...
		//  surtout si vous n'utilisez pas filter_input(...)  
//					$nom = strtoupper($connexion->quote($_POST['nom']));
//					$prenom = $connexion->quote($_POST['prenom']);
		$insertPersQuery = "INSERT INTO personne(nom,prenom) VALUES ('$nom','$prenom');";
		//var_dump($insertPersQuery);

		$count = $connexion->exec($insertPersQuery);
	    }
	    catch(Exception $e)
	    {
		header("location: ../error/dbError.html") ;
	    }
	}
    }
?>
<body>
    <h1 class="titre">BdD avec PDO ...</h1>	
    <div id="cadre_saisie">
	<form method="post" action="<?php echo (filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL)); ?>" id="formulaire">
	    <fieldset><legend>Votre identité:</legend>
		<table>
		<tr>
		    <th><label for="prenom">Prénom: </label></th><td><input id="nom" type="text" name="prenom" required="required" maxlength="30" size="30" /></td>
		</tr><tr>
		    <th><label for="nom">Nom: </label></th><td><input id="prenom" type="text" name="nom" required="required" maxlength="30" size="30" /></td>
		</tr></table>
		<input type="submit" value="Enregistrer" />
	    </fieldset>
	</form>
    </div>
    <div id="cadre_affichage">
    <?php
	try
	{
	    $connexion = new PDO('mysql:host='.$hostName.';dbname='.$dbName, $userName, $userPassword,
		array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	    $selectPersQuery = "SELECT * FROM personne ORDER BY nom";
	    $resultats=$connexion->query($selectPersQuery);

	    echo '<table summary="Liste des personnes">';
	    while($unePers = $resultats->fetch(PDO::FETCH_OBJ))
	    {
		echo '<tr><td>'.$unePers->id_pers.'</td><td>'.$unePers->prenom.'</td><td>'.strtoupper($unePers->nom).'</td></tr>'."\n";
	    }
	    echo '</table>';

	    $resultats->closeCursor(); // ferme les résultats
	}
	catch(Exception $e)
	{
	    header("location: ../error/dbError.html") ;
	}
    ?>
    </div>
</body>
</html>
