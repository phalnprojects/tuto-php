<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>Démo MySQLi</title>
</head>
    <?php
        require_once '../connect.inc.php';
        
	if(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS) == 'POST')
	{
	    $nom = ($nom = strtoupper(filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS))) ? substr(strtoupper($nom), 0, 30) : "";
	    $prenom = ($prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS))? substr($prenom, 0, 30) : "";

	    if($nom != "" && $prenom != "")
	    {
		//  Instanciation
		$connexion = new mysqli($hostName, $userName, $userPassword, $dbName);

		/* Vérification de la connexion */
		if ($connexion->connect_error)
		{
		    header("location: ../error/dbError.html") ;		    
		}
		else
		{	
		    //  ATTENTION! Ne pas oublier d'échapper les chaînes si vous n'avez pas utilisé les filtres...
//				$nom = strtoupper($connexion->real_escape_string($_POST['nom']));
//				$prenom = $connexion->real_escape_string($_POST['prenom']);
		    $insertPersQuery = "INSERT INTO personne(nom,prenom) VALUES ('$nom','$prenom')";

		    //var_dump($insertPersQuery);

		    $reqOk = $connexion->query($insertPersQuery);
		    $connexion->close();
		    
		    if(!$reqOk)
			header("location: ../error/dbError.html") ;		    
		}
	    }
        }
    ?>
<body>
    <h1 class="titre">BdD avec MySQLi ...</h1>	
    <div id="cadre_saisie">
	<form method="post" action="<?php echo (filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL)); ?>" id="formulaire">
	    <fieldset><legend>Votre identité:</legend>
		<label for="prenom">Prénom: </label><input id="nom" type="text" name="prenom" required="required" maxlength="30" size="30" /><br/>
		<label for="nom">Nom: </label><input id="prenom" type="text" name="nom" required="required" maxlength="30" size="30" />
		<br/>
		<input type="submit" value="Enregistrer" />
	    </fieldset>
	</form>
    </div>
    <div id="cadre_affichage">
    <?php
	$connexion = new mysqli($hostName, $userName, $userPassword, $dbName);
	$connexion->set_charset("utf8");

	/* Vérification de la connexion */
	if ($connexion->connect_error)
	{
	    header("location: ../error/dbError.html") ;		    
	}

	$selectPersQuery = "SELECT * FROM personne ORDER BY nom";
	$resultats=$connexion->query($selectPersQuery);
	
//	var_dump($resultats);
    ?>
	<h2>Tableau associatif</h2>
	<table summary="Liste des personnes">
	<?php while($unePers = $resultats->fetch_assoc()): ?>

	<tr>
	    <td><?= $unePers["id_pers"]; ?></td>
	    <td><?= $unePers["prenom"]; ?></td>
	    <td><?= strtoupper($unePers["nom"]); ?></td>
	</tr>
	<?php endwhile; ?>
	</table>

	<h2>Objets</h2>				
	<table summary="Liste des personnes">
	<?php 
	$resultats->data_seek(0);	// Pour se remettre au début des résultats
	
	while($unePers = $resultats->fetch_object()): ?>
	<tr>
	    <td><?= $unePers->id_pers; ?></td>
	    <td><?= $unePers->prenom; ?></td>
	    <td><?= strtoupper($unePers->nom); ?></td>
	</tr>
	<?php endwhile; ?>
	</table>


	<h2>Direct</h2>				
	<table summary="Liste des personnes">
	<?php 
	$resultats->data_seek(0);	// Pour se remettre au début des résultats
	
	foreach($resultats as $unePers): ?>
	<tr>
	    <td><?= $unePers["id_pers"]; ?></td>
	    <td><?= $unePers["prenom"]; ?></td>
	    <td><?= strtoupper($unePers["nom"]); ?></td>
	</tr>
	<?php endforeach; ?>
	</table>
	
	<?php
	    $resultats->close(); // ferme les résultats
	    $connexion->close();
	?>
    </div>
</body>
</html>
