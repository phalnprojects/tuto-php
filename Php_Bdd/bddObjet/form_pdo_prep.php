<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>Démo PDO</title>
</head>
<?php
    include_once 'connect_pdo.inc.php';

    try
    {
	$connexion = connectPDO();
	$reqInsPrep = prepInsertPersQuery($connexion);
	$reqSelect = prepSelectPersQuery($connexion);

	if('POST' === filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS))
	{
	    $nom = ($nom = strtoupper(filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS))) ? substr(strtoupper($nom), 0, 30) : "";
	    $prenom = ($prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS))? substr($prenom, 0, 30) : "";

	    if($nom != "" && $prenom != "")
	    {
		$reqInsPrep->bindParam(':nom', $nom, PDO::PARAM_STR);
		$reqInsPrep->bindParam(':prenom', $prenom, PDO::PARAM_STR);
		$reqInsPrep->execute();
	    }
	}
    }
    catch(Exception $e)
    {
	header("location: ../error/dbError.html");
    }
?>
<body>
    <h1 class="titre">BdD avec les requêtes préparées de PDO ...</h1>	
    <div id="cadre_saisie">
	<form method="post" action="<?php echo (filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL)); ?>" id="formulaire">
	    <fieldset><legend>Votre identité:</legend>
		<label for="prenom">Prénom: </label><input id="nom" type="text" name="prenom" required="required" maxlength="30" size="30" /><br/>
		<label for="nom">Nom: </label><input id="prenom" type="text" name="nom" required="required" maxlength="30" size="30" />
		<br/>
		<input type="submit" value="Enregistrer" />
	    </fieldset>
	</form>
    </div>
    <div id="cadre_affichage">
    <?php
	try
	{
	    $reqSelect->execute();

	    echo '<table summary="Liste des personnes">';
	    while($unePers = $reqSelect->fetch(PDO::FETCH_OBJ))
	    {
		echo '<tr><td>'.$unePers->id_pers.'</td><td>'.$unePers->prenom.'</td><td>'.strtoupper($unePers->nom).'</td></tr>'."\n";
	    }
	    echo '</table>';                
	}
	catch(Exception $e)
	{
	    header("location: ../error/dbError.html");
	}
    ?>
    </div>
</body>
</html>
