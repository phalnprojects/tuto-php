<?php	
    function connectPDO()
    {
	require_once '../connect.inc.php';
	$connexion = new PDO('mysql:host='.$hostName.';dbname='.$dbName, $userName, $userPassword,
		array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        return $connexion;
    }
    
    function prepInsertPersQuery(PDO $connexion)
    {
        $insertPersQuery="INSERT INTO personne(nom,prenom) VALUES (:nom, :prenom)";
        return $connexion->prepare($insertPersQuery);
    }
    
    function prepSelectPersQuery(PDO $connexion)
    {
        $selectPersQuery = "SELECT * FROM personne ORDER BY nom";
        return $connexion->prepare($selectPersQuery);
    }
