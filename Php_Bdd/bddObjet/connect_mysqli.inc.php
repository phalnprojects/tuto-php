<?php
function connectMySQLi()
{
    require_once '../connect.inc.php';

    $connexion = new mysqli($hostName, $userName, $userPassword, $dbName);
    /* V�rification de la connexion */
    if ($connexion->connect_error)
    {
	header("location: ../error/dbError.php");    }
    return $connexion;
}

function prepInsertPersQuery(mysqli $connexion)
{
    $insertPersQuery="INSERT INTO personne(nom,prenom) VALUES (?, ?)";
    return $connexion->prepare($insertPersQuery);
}

function prepSelectPersQuery(mysqli $connexion)
{
    $selectPersQuery = "SELECT * FROM personne ORDER BY nom";
    return $connexion->prepare($selectPersQuery);
}
	