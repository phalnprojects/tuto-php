-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 30 Mai 2013 à 07:32
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `test_gestmedia`
--

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `id_pers` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL DEFAULT '',
  `prenom` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_pers`),
  KEY `nom` (`nom`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Contenu de la table `personne`
--

INSERT INTO `personne` (`id_pers`, `nom`, `prenom`) VALUES
(20, 'CULAIRE', 'Laurie'),
(24, 'ZETOFRÉ', 'Mélanie'),
(23, 'DEUJOUR', 'Adam'),
(22, 'DEUF', 'John'),
(31, 'SOURCE', 'Aude'),
(8, 'O''GARAG', 'Mélodie'),
(9, 'ENCEQUENDISELONG', 'Cécile'),
(30, 'FRAICHIT', 'Sarah'),
(11, 'OCHON', 'Paul'),
(29, 'TALU', 'Jean'),
(16, 'ASSOIE', 'Phil'),
(17, 'DUCABLE', 'Jean-Raoul'),
(19, 'DELAMAIN', 'Marc'),
(25, 'NETOFRIGO', 'Jessica'),
(26, 'ONCITERNE', 'Camille'),
(28, 'CLETTE', 'Lara');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
