<?php
    require_once '../connect.inc.php';
    
    //  Insertion dans la base de données d'une nouvelle personne d'après les informations venant du formulaire.
    //  
    //  Cette partie de code sera exécuté seulement lors d'une requète POST, c'est à dire lorsque le formulaire sera soumis.
    //  Insertion dans la base de données dune nouvelle personne d'aprés les informations venant du formulaire
    if(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS) == 'POST')
    {
	// Récupération sécurisée des champs du formulaire
	// On en profite pour mettre en majuscule si nécessaire
	// et à vérifier la longueur (identique à la longueur du champ dans la bdd)
	$nom = ($nom = strtoupper(filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS))) ? substr(strtoupper($nom), 0, 30) : "";
	$prenom = ($prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS))? substr($prenom, 0, 30) : "";

	// L'insertion ne se fait que si les champs ne sont pas vide...
	// Même si les champs sont marqués "required" dans le formulaire, car on est JAMAIS sûr de la provenance...
	if($nom != "" && $prenom != "")
	{
	    //  Connexion à la base de donnée. Les information sont dans le fichier connect...
	    $connexion = mysqli_connect($hostName, $userName, $userPassword, $dbName);

	    /* Vérification de la connexion */
	    if (mysqli_connect_error())
	    {
//		    die('Erreur de connexion ('
//				    . mysqli_connect_errno() . ') '
//				    . mysqli_connect_error());
		//  En toute rigueur, en production, on évite les messages d'erreur standards: trop bavard... 
		//  On compose plutôt des pages d'erreurs...
		header("location: ../error/dbError.html") ;		    
	    }
	    else
	    {	
		//  ATTENTION! Ne pas oublier d'échapper les chaines composant une requète...
		//  surtout si vous n'utilisez pas filter_input(...)
		$nom = mysqli_real_escape_string($connexion, $nom);
		$prenom = mysqli_real_escape_string($connexion, $prenom);

		//  Composition de la requète SQL dans une chaîne de caractères, pratique à tracer en développement...
		//  et à essayer avec PHPMyAdmin. Il faut souvent faire attention aux quotes autour des valeurs...
		$insertPersQuery = "INSERT INTO personne(nom,prenom) VALUES ('$nom','$prenom')";
		//var_dump($insertPersQuery);

		//  Exécution de la requète. Si une erreur, redirection vers la page d'erreur.
		$reqOk = mysqli_query($connexion, $insertPersQuery);
		if(!$reqOk)
		    header("location: ../error/dbError.html") ;		    

		//  Fermeture de la connexion
		mysqli_close($connexion);
	    }
	}
    }
    if(!$reqOk)
	header("location: ../error/dbError.html") ;
    else
	header("location: ./index.php") ;
