<!DOCTYPE html>
<?php
    require_once '../connect.inc.php';
    
    $connexion = mysqli_connect($hostName, $userName, $userPassword, $dbName);

    // Vérification de la connexion
    if (mysqli_connect_error())
    {
	header("location: ../error/dbError.html") ;		    
    }

    //  Préparation des requètes
    $reqInsPrep = mysqli_prepare($connexion, "INSERT INTO personne(nom,prenom) VALUES (?, ?)");
    $reqSelect = mysqli_prepare($connexion, "SELECT * FROM personne WHERE nom LIKE ? ORDER BY nom");
    if($reqInsPrep === FALSE || $reqSelect === FALSE)
	header("location: ../error/dbError.html") ;		    
    
    $masque = '%';
?>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>Démo MySQLi</title>
</head>
<body>
    <?php
	//  Cette partie de code n'est exécutée que si la page est appellée par une requète POST
	if(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS) == 'POST')
	{
	    // Récupération sécurisée des champs du formulaire
	    // On en profite pour mettre en majuscule si nécessaire
	    // et à vérifier la longueur (identique à la longueur du champ dans la bdd)
	    $nom = ($nom = strtoupper(filter_input(INPUT_POST, 'nom', FILTER_SANITIZE_SPECIAL_CHARS))) ? substr(strtoupper($nom), 0, 30) : "";
	    $prenom = ($prenom = filter_input(INPUT_POST, 'prenom', FILTER_SANITIZE_SPECIAL_CHARS))? substr($prenom, 0, 30) : "";

	    // L'insertion ne se fait que si les champs ne sont pas vide...
	    // Même si les champs sont marqués "required" dans le formulaire, car on est JAMAIS sûr de la provenance...
	    if($nom != "" && $prenom != "")
	    {
		//  Association des valeurs aux tags
		if(mysqli_stmt_bind_param($reqInsPrep, 'ss', $nom, $prenom))
		{
		    //  Exécution de la requète préparée.
		    if(mysqli_stmt_execute($reqInsPrep) === FALSE)
			header("location: ../error/dbError.html");		    
		    else
		    {
			//  Affiche le nombre de lignes affectées et ferme le traitement
			//echo '<p>'. mysqli_stmt_affected_rows($reqInsPrep) . ' ligne(s) insérée(s).</p>';
			mysqli_stmt_close($reqInsPrep);
		    }
		}
	    }
	}
    ?>
    <h1 class="titre">BdD avec les requêtes préparées de MySQLi ...</h1>	
    <div id="cadre_saisie">
	<!-- Ici, le formulaire est soumis à lui-même. Voir attribut action. -->
	<form method="post" action="<?= filter_input(INPUT_SERVER, 'PHP_SELF', FILTER_VALIDATE_URL); ?>" id="formulaire">
	    <fieldset><legend>Votre identité:</legend>
		<label for="prenom">Prénom: </label>
		    <input id="nom" type="text" name="prenom" required="required" maxlength="30" size="30" /><br/>
		<label for="nom">Nom: </label>
		    <input id="prenom" type="text" name="nom" required="required" maxlength="30" size="30" />
		<br/>
		<input type="submit" value="Enregistrer" />
	    </fieldset>
	</form>
    </div>
    <div id="cadre_affichage">
    <?php
	//  Cette partie de code est exécutée que la page soit appellée par une requète GET ou POST
	//  Affiche le contenu de la table personne.
	//  	
	//  Association des valeurs aux tags
	if(mysqli_stmt_bind_param($reqSelect, 's', $masque))
	{
	    //  Exécution de la requéte
	    if(mysqli_stmt_execute($reqSelect) === FALSE)
	    {
		header("location: ../error/dbError.html");
	    }
	}		
	?>
	<h2>Tableau associatif</h2>
	<table summary="Liste des personnes">
	<?php
	    //  Récupération des résultats et extraction ligne par ligne en tableaus associatifs
	    $resultats = mysqli_stmt_get_result($reqSelect);
	    while($unePers = mysqli_fetch_assoc($resultats)):
	?>
	    <tr>
		<td><?= $unePers["id_pers"]; ?></td>
		<td><?= $unePers["prenom"]; ?></td>
		<td><?= strtoupper($unePers["nom"]); ?></td>
	    </tr>
	<?php endwhile; ?>
	</table>
	<?php
	    mysqli_stmt_close($reqSelect); // Libère les ressources occupées pour les résultats
	    mysqli_close($connexion);
    ?>
    </div>
</body>
</html>
