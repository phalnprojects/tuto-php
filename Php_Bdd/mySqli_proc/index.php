<!DOCTYPE html>
<?php
    //  Pour les informations de connexion (serveur, bdd, user, password)
    require_once '../connect.inc.php';
?>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <title>Démo MySQLi</title>
</head>
<body >
    <h1 class="titre">BdD avec MySQLi Procédural...</h1>
    <div id="cadre_saisie">
	<h2>Liste des personnes:</h2>	
	<?php
	    // Connexion à la bdd
	    $connexion = mysqli_connect($hostName, $userName, $userPassword, $dbName);

	    /* Vérification de la connexion */
	    if (mysqli_connect_error())
	    {
    //		    die('Erreur de connexion ('
    //				    . mysqli_connect_errno() . ') '
    //				    . mysqli_connect_error());
		    //  En toute rigueur, en production, on évite les messages d'erreur standards: trop bavard... 
		    //  On compose plutôt des pages d'erreurs...
		    header("location: ../error/dbError.html") ;		    
	    }

	    //  Composition de la requète SQL dans une variable, puis exécution de la requète
	    $selectPersQuery = "SELECT * FROM personne ORDER BY nom";
	    $resultats = mysqli_query($connexion, $selectPersQuery);
	    if(!$resultats)
		header("location: ../error/dbError.html") ;		    
	?>
	<!--  On va afficher le contenu de $resultat dans une table html  -->
	<h2>Tableau associatif</h2>
	<table summary="Liste des personnes">
	<?php 
	    //  Parcours (mysqli_fetch) ligne par ligne du "paquet" de données contenues dans $resultat.
	    //  La ligne extraite est dans $unePers sous forme de tableau associatif (fetch_assoc)
	    while($unePers = mysqli_fetch_assoc($resultats)): 
	?>
	<tr>
	    <!--  Les clés du tableau associatif sont les noms des champs dans la bdd ou les allias définis dans la requète sql  -->
	    <td><?= $unePers["id_pers"]; ?></td>
	    <td><?= $unePers["prenom"]; ?></td>
	    <td><?= strtoupper($unePers["nom"]); ?></td>
	</tr>
	<?php 
	    endwhile; 
	?>
	</table>

	<h2>Objets</h2>				
	<table summary="Liste des personnes">
	<?php 
	    mysqli_data_seek($resultats, 0);	// Pour se remettre au début des résultats...

	    // Même chose, mais en utilisant une extraction sous forme d'objet avec mysqli_fetch_object()
	    while($unePers = mysqli_fetch_object($resultats)):
	?>
	<tr>
	    <td><?= $unePers->id_pers; ?></td>
	    <td><?= $unePers->prenom; ?></td>
	    <td><?= strtoupper($unePers->nom); ?></td>
	</tr>
	<?php 
	    endwhile; 
	?>
	</table>

	<?php
	    mysqli_free_result($resultats); // Libère les ressources occupées pour les résultats
	    mysqli_close($connexion);
	?>
	<p></p>
	<p><a href="../index.php">Retour à l'index.</a></p>
    </div>
</body>
</html>
