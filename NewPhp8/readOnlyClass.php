<?php
declare(strict_types=1);

class PersonneOld
{
    private string $nom;
    public readonly string $prenom;
    
    public function getNom() : string
    {
        return $this->nom;
    }
    public function __construct(string $nom, string $prenom)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
    }
}

/**
 * Classe "readonly" PHP >= 8.2
 */
readonly class PersonneNew
{
    public string $nom;
    public string $prenom;
    
    public function __construct(string $nom, string $prenom)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
    }
}

$p1 = new PersonneOld('Old', 'Man');
echo "<p>p1: $p1->prenom {$p1->getNom()}</p>";

$p2 = new PersonneNew('New', 'Woman');
echo "<p>p1: $p2->prenom $p2->nom</p>";