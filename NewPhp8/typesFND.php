<?php
declare(strict_types=1);

readonly class C1
{
    public string $c;
    public function __construct(?string $param=null)
    {
        $this->c = (!is_null($param)) ? $param : "Default in C1";
    }
}

readonly class C2 extends C1
{
    public function __construct(?string $param=null)
    {
        parent::__construct((!is_null($param)) ? $param : "Default in C2");
    }
}

readonly class D extends C1
{
    public string $c;
    public function __construct(?string $param=null)
    {
        parent::__construct((!is_null($param)) ? $param : "Default in D");
    }
}

class BeforeFDN {
    public static function bar(mixed $entity) {
        if ((($entity instanceof C1) && ($entity instanceof C2)) || ($entity === null)) {
            return $entity;
        }
        throw new Exception('Invalid entity');
    }
}

/**
 * Classe "Types forme normale disjonctive" PHP >= 8.2
 */
class WithFDN
{
    public static function bar((C1&C2)|null $entity) {
        return $entity;
    }
    
}

$c1 = new C1();
$c2 = new C2();
$d = new D();

try {
    $x = BeforeFDN::bar($c1);
    echo "<p>1) $x->c</p>";
} catch (Throwable $e) {
    echo "<p>1) {$e->getMessage()}</p>";
}

try {
    $x = BeforeFDN::bar($c2);
    echo "<p>2) $x->c</p>";
} catch (Throwable $e) {
    echo "<p>2) {$e->getMessage()}</p>";
}

try {
    $x = BeforeFDN::bar($d);
    echo "<p>3) $x->c</p>";
} catch (Throwable $e) {
    echo "<p>3) {$e->getMessage()}</p>";
}

try {
    $x = BeforeFDN::bar(null);
    echo ($x!==null)?"<p>4)$x->c</p>":"<p>4) null</p>";
} catch (Throwable $e) {
    echo "<p>4) {$e->getMessage()}</p>";
}

try {
    $x = WithFDN::bar($c1);
    echo "<p>5) $x->c</p>";
} catch (Throwable $e) {
    echo "<p>5) {$e->getMessage()}</p>";
}

try {
    $x = WithFDN::bar($c2);
    echo "<p>6) $x->c</p>";
} catch (Throwable $e) {
    echo "<p>6) {$e->getMessage()}</p>";
}

try {
    $x = WithFDN::bar($d);
    echo "<p>7) $x->c</p>";
} catch (Throwable $e) {
    echo "<p>7) {$e->getMessage()}</p>";
}

try {
    $x = WithFDN::bar(null);
    echo ($x!==null)?"<p>8)$x->c</p>":"<p>8) null</p>";
} catch (Throwable $e) {
    echo "<p>8) {$e->getMessage()}</p>";
}
