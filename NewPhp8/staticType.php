<?php
//  Utilisation du typage fort.
//  Commenter et regarder la différence...
declare(strict_types=1);

class ParentClass
{
    public static function getSelf():self 
    {
	return new self();
    }
    
    public static function getStatic():static 
    {
	return new static();
    }
}

class DerivedClass extends ParentClass {}
?>

<h2>getSelf:</h2>
<h3>Sur ParentClass:</h3>
<?php
$obj = ParentClass::getSelf();
var_dump($obj);
?>
<h3>Sur DerivedClass:</h3>
<?php
$obj = DerivedClass::getSelf();
var_dump($obj);
?>

<h2>getStatic:</h2>
<h3>Sur ParentClass:</h3>
<?php
$obj = ParentClass::getStatic();
var_dump($obj);
?>
<h3>Sur DerivedClass:</h3>
<?php
$obj = DerivedClass::getStatic();
var_dump($obj);
?>