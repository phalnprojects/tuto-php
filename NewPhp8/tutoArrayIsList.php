<?php
var_dump(array_is_list([]));
var_dump(array_is_list([1, 2, 3]));
var_dump(array_is_list(['rock', 2, 3]));
var_dump(array_is_list(['rock', 'scissor']));
var_dump(array_is_list([0 => 'rock', 'scissor']));
var_dump(array_is_list([0 => 'rock', 1 => 'scissor']));

var_dump(array_is_list([1 => 'rock', 'scissor'])); // faux, ne commence pas par 0
var_dump(array_is_list([1 => 'rock', 0 => 'scissor'])); // faux, pas en ordre
var_dump(array_is_list([0 => 'rock', 'suit' => 'paper'])); //faux, clés non entières
var_dump(array_is_list([0 => 'rock', 2 => 'paper'])); //faux, non séquentiel