<?php
//  Utilisation du typage fort.
//  Commenter et regarder la différence...
declare(strict_types=1);

class NullSafeSampleClass
{

    private bool $nullValue = false;
    private ?DateTime $val;

    public function generateValOrNull(): ?DateTime {
	$this->val = ($this->nullValue) ? new DateTime() : null;
	$this->nullValue = !$this->nullValue;
	return $this->val;
    }

}
?>

<h2>opérateur nullSafe:</h2>
<?php
$obj = new NullSafeSampleClass();

$val = 'NullSafe sample 1';
$val = $obj->generateValOrNull()?->format('Y-m-d H:m:s');
var_dump($val);

$val = 'NullSafe sample 2';
$val = $obj->generateValOrNull()?->format('Y-m-d H:m:s');
var_dump($val);

try {
    $val = 'NullSafe sample 3';
    $val = $obj->generateValOrNull()->format('Y-m-d H:m:s');
    var_dump($val);
} catch (Throwable $ex) {
    var_dump($ex);
}
?>
