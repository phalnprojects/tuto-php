<?php

declare(strict_types=1);

function incremente(int|float $val) : int|float {
    return $val + 1;
}

function nomme(int $p1, string $p2, float $p3) {
    echo "<p>P1: $p1<br/>P2: $p2<br/>P3: $p3</p>";
}

echo'<p>incremente(10):</p>';
$val = incremente(10);
var_dump($val);

echo'<p>incremente(22.2):</p>';
$val = incremente(22.2);
var_dump($val);

echo'<p>nomme(10, "str1", 3.14):</p>';
nomme(10, "str1", 3.14);
nomme(p2:"str1", p3:3.14, p1:10);
nomme(p3:3.14, p1:10, p2:"str1");

