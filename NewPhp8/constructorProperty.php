<?php
declare(strict_types=1);

class ConstructPropertyPromoteClass {

    public readonly int $uid;

    public function getNumber(): int|float {
        return $this->number;
    }

    public function __construct(
            private int|float $number,
            protected DateTime $horodatage,
            int $other
    ) {
        $this->uid = $other;
    }

    public function modifyUid(int $value): int {
        $this->uid = $value;
    }

}
?>

<h2>Avec Entier:</h2>
<?php
$obj = new ConstructPropertyPromoteClass(66, new DateTime(), 22);
var_dump($obj);
echo'<br/>';
$var = $obj->getNumber();
var_dump($var);
?>

<h2>Avec Réel:</h2>
<?php
$obj = new ConstructPropertyPromoteClass(3.14, new DateTime('2020-01-01'), 33);
var_dump($obj);
echo'<br/>';
$var = $obj->getNumber();
var_dump($var);
?>

<h2>Avec les arguments nommés:</h2>
<?php
$d = new DateTime('2019-10-01');
$obj = new ConstructPropertyPromoteClass(horodatage: $d, other: 99, number: 6.28);
var_dump($obj);
echo'<br/>';
$var = $obj->getNumber();
var_dump($var);
?>

<h2>Tentative modif uid</h2>
<?php
try {
    $obj->uid = 99;
} catch (Throwable $ex) {
    echo"<p>Modif uid impossible, readonly</p>";
}

try {
    $obj->modifyUid(666);
} catch (Throwable $ex) {
    echo"<p>Modif uid impossible, même depuis la classe...</p>";
}

