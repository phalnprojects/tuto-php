<?php
//  Utilisation du typage fort.
//  Commenter et regarder la différence...
declare(strict_types=1);

class UnionClass
{

    private int|float $number;

    public function getNumber(): int|float {
	return $this->number;
    }

    public function setNumber(int|float $number) {
	$this->number = $number;
	return $this;
    }

    public function __construct(int|float $number) {
	$this->setNumber($number);
    }

}
?>

<h2>Avec Entier:</h2>
<?php
$obj = new UnionClass(66);
var_dump($obj);
echo'<br/>';
$var = $obj->getNumber();
var_dump($var);
?>

<h2>Avec Réel:</h2>
<?php
$obj = new UnionClass(3.14);
var_dump($obj);
echo'<br/>';
$var = $obj->getNumber();
var_dump($var);
?>

<h2>Avec une string:</h2>
<?php
try {
    $obj = new UnionClass('toto');
    var_dump($obj);
    echo'<br/>';
    $var = $obj->getNumber();
    var_dump($var);
} catch (Throwable $ex) {
    var_dump($ex);
}
?>

<h2>Avec un nombre en string:</h2>
<?php
try {
    $obj = new UnionClass('99.9');
    var_dump($obj);
    echo'<br/>';
    $var = $obj->getNumber();
    var_dump($var);
} catch (Throwable $ex) {
    var_dump($ex);
}
?>
