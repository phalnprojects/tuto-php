<?php
declare(strict_types=1);

enum Couleur:string {
    case pique = '♠';
    case carreau = '♦';
    case coeur = '♥';
    case trefle = '♣';
}

var_dump(Couleur::cases());
var_dump(Couleur::cases()[0]);
var_dump(Couleur::cases()[rand(0, count(Couleur::cases())-1)]);

enum Carte:string {
    case un = '1';
    case deux = '2';
    case trois = '3';
    case quatre = '4';
    case cinq = '5';
    case six = '6';
    case sept = '7';
    case huit = '8';
    case neuf = '9';
    case dix = '10';
    case valet = 'Valet';
    case dame = 'Dame';
    case roi = 'Roi';
}

$valeurCarte = Carte::cases()[rand(1, count(Carte::cases())-1)];
var_dump($valeurCarte);
$carte = $valeurCarte->value . Couleur::cases()[rand(0, count(Couleur::cases())-1)]->value;
echo "<p>$carte</p>";

function tireUneCarte():string
{
    $valeurCarte = Carte::cases()[rand(1, count(Carte::cases())-1)];
    $carte = $valeurCarte->value . Couleur::cases()[rand(0, count(Couleur::cases())-1)]->value;
    return $carte;
}

echo '<p>'.tireUneCarte().'</p>';