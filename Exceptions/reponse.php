<?php
declare(strict_types=1);

require_once './desFonctions.php';

try {
    echo'<h1>op1()</h1>';
    $var = op1($_GET['a'], $_GET['b']);
    dump_var($var,true, 'Résultat de op1():');
    
    $filter = include './tableauFiltres.php';
    
    $get = filter_input_array(INPUT_GET, $filter);
    
    try {
	echo'<h1>op2()</h1>';
	$var = op2($get['a'], $get['b']);
	dump_var($var,true, 'Résultat de op2():');
    }
    catch (TypeError $exc) {
	echo "<p>Interception d'une erreur de type des paramètres dans op2()...</p>";
	echo "<p>L'application continue sans avoir terminé op2().</p>";
	echo "<p>Type: ". get_class($exc) ." Message: {$exc->getMessage()}</p>";
	echo $exc->getTraceAsString();
    } 
    
    try {
	echo'<h1>op3()</h1>';
	$var = op3($get['a'], $get['b']);
	dump_var($var,true, 'Résultat de op3():');
    }
    catch (MyException $exc) {
	echo "<p>Interception de MyException, op3 pas terminée, mais le programme peut continuer</p>";
	echo "<p>Type: ". get_class($exc) ." Message: {$exc->getMessage()}</p>";
	echo $exc->getTraceAsString();
    } 
    
    echo'<h1>op4()</h1>';
    $var = op4($get['a'], $get['b']);
    dump_var($var,true, 'Résultat de op4():');    
} 
catch (MyException $exc) {
    echo "<p>Interception de MyException par le programme.</p>";
    echo "<p>Type: ". get_class($exc) ." Message: {$exc->getMessage()}</p>";
    echo $exc->getTraceAsString();    
} 
catch (Exception $exc) {
    echo "<p>Interception de Exception par le programme.</p>";
    echo "<p>Type: ". get_class($exc) ." Message: {$exc->getMessage()}</p>";
    echo $exc->getTraceAsString();
} 
catch (Throwable $exc) {
    echo "<p>Interception de Throwable par le programme.</p>";
    echo "<p>Type: ". get_class($exc) ." Message: {$exc->getMessage()}</p>";
    echo $exc->getTraceAsString();
} 
finally {
    echo "<p></p><p>C'est finally!!!</p>";
}

echo "<p></p><p>Cette partie n'est pas gérée...</p>";
