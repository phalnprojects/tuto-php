<?php
error_reporting(E_ALL);
ini_set('display_errors','Off');  

define('DUMP', FALSE);


function dump_var($var, $dump=TRUE, $msg=null) {
    if($dump) {
	if($msg)
	    echo"<p><strong>$msg</strong></p>";
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
    }
}

class MyException extends Exception {    
}

function op1($a, $b) {
    $res = $a / $b;
    dump_var($res,DUMP, '$res dans op1:');
    return $res;
}

function op2(int $a, int $b) : float {
    $res = $a / $b;
    dump_var($res,DUMP, '$res dans op2:');
    return $res;
}

function op3(int $a, int $b) : float {
    $res = $a / $b;
    dump_var($res,DUMP, '$res dans op3:');
    if($res === INF)
	throw new MyException("$res n'est pas significatif dans op3().");
	
    return $res;
}

function op4(int $a, int $b) : float {
    if($b < 0)
	throw new MyException("$b n'est pas une valeur valide dans op4().");
    $res = op3($a, $b);
    dump_var($res,DUMP, '$res dans op4:');
    return $res;
}

