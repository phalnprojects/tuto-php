<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/stylepropre.css" />
    <title>Upload</title>
</head>
<body>
    <?php
	define('MAX_FILE_SIZE', '100000'); //  Taille maximale d'un fichier
    ?>
    <header id="entete">
	<p>ORT-Sup' Lyon</p>
    </header>
    <div id="corps">
	<h1>Choisir le fichier:</h1>
	<form id="formUpload" enctype="multipart/form-data" method="post" action="UploadAction.php">
	    <fieldset><legend>Choix du fichier</legend>
		<label for="champFichier">Votre fichier local: </label> 
		<!-- A mettre avant le champ input files -->
		<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo MAX_FILE_SIZE; ?>" /> 
		<input type="file" id="champFichier" name="champFichier" class="champValide"/><br/>
		<input type="submit" name="uploader" value="Uploader" />
	    </fieldset>
	</form>
    </div>	
    <footer id="pied">
	<p>PhAln.info for CS2i - Septembre 2017 -</p>
    </footer>
</body>
</html>