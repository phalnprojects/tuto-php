<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/stylepropre.css" />
    <title>Upload</title>
</head>
<body>
    <?php
	define('MAX_FILE_SIZE', '100000'); //  Taille maximale d'un fichier

	/**
	 * Fonction de remplacement des caractères accentués dans une chaîne. Fonctionne en utf-8.
	 * Utilise des RegEx.
	 * http://www.weirdog.com/blog/php/supprimer-les-accents-des-caracteres-accentues.html
	 * @param type $str la chaîne à traiter
	 * @param type $charset le charset à utiliser
	 * @return string la chaîne traitée
	 */
	function wd_remove_accents($str, $charset='utf-8')
	{
	    $str = htmlentities($str, ENT_NOQUOTES, $charset);

	    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
	    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
	    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

	    return $str;
	}

	/**
	 * Fonction de remplacement des caractères accentués dans une chaîne. Fonctionne en utf-8
	 * http://www.lecoindunet.com/remplacer-les-caracteres-accentues-dune-chaine-en-php-72
	 * @param type $str la chaîne à traiter
	 * @return string la chaîne traitée
	 */
	function remove_accent($str)
	{
	    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð',
			'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã',
			'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ',
			'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ',
			'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę',
			'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī',
			'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ',
			'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ',
			'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 
			'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 
			'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ',
			'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');

	    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O',
			'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c',
			'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u',
			'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D',
			'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g',
			'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K',
			'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o',
			'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S',
			's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W',
			'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i',
			'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
	    return str_replace($a, $b, $str);
	} 	
    ?>
    <header id="entete">
	<p>ORT-Sup' CS2i Lyon</p>
    </header>
    <div id="corps">
	<h1>Upload</h1>
	<fieldset><legend>Téléversement</legend>
	    <?php
//		var_dump($_FILES);

		if(filter_input(INPUT_POST, 'uploader', FILTER_SANITIZE_SPECIAL_CHARS) == 'Uploader' ) // si formulaire soumis
		{
		    $fichierTempo = $_FILES['champFichier']['tmp_name'];
		    if(empty($fichierTempo) OR !is_uploaded_file($fichierTempo) )
		    {
			echo "<p>Le fichier est introuvable</p>";
		    }
		    else
		    {
			// dossier où sera déplacé le fichier une fois téléversé
			$dossierDestination = './uploadedFiles/'; 

			//  Le nom d'origine saisi dans le formulaire
			$nomOrigine = filter_var($_FILES['champFichier']['name'], FILTER_SANITIZE_SPECIAL_CHARS);

			// vérifier que le nom du fichier ne comporte pas le caractère null, autre caractère de contrôle ou slash et backslash
			//if( true) 
			if( preg_match('#[\x00-\x1F\x7F-\x9F/\\\\]#', $nomOrigine) )
			{
			    exit("<p>Nom de fichier non valide</p>");
			}

			//  ou alors, on remplace tout ce qui gène dans le nom...
			//  en commencant par les lettres accentuées

			//  Simple, mais ne fonctionne pas en utf-8, les caractères accentués étant sur plusieurs octets...
			//  $nomOrigine = strtr($nomOrigine,
			//	 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
			//	 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy'); 

			//  Utiliser une des fonctions présentées plus haut
			$nomOrigine = remove_accent($nomOrigine);						

			//puis tout ce qui n'est pas une lettre ou un chiffre est remplacé par un -.
			$nomOrigine = preg_replace('/([^.a-z0-9]+)/i', '-', $nomOrigine);

			//  On s'assure de la taille du fichier...
			//  Il semblerait que l'utilisation de $_FILES['champFichier']['size'] puisse être une faille de sécurité
			//  Notez que la taille est limitée dans php.ini, $MAX_FILE_SIZE doit être inférieur 
			if(filesize($fichierTempo) < MAX_FILE_SIZE)
			{
			    $extensionOk = TRUE;
			    
			    //  Sépare les différents éléments du nom du fichier d'origine, met l'extension de fichier en minuscule
			    $elementsChemin = pathinfo($nomOrigine);
			    $extensionFichier = strtolower($elementsChemin['extension']);

			    //  Vérification de l'extension.
			    //  Trois possibilité vous sont présentées, il peut y en avoir d'autre, par exemple avec les magic number.
			    //  Voir la liste des extensions et de magic number sur https://en.wikipedia.org/wiki/List_of_file_signatures

			    //******************************************************************************
			    //  Simplement avec les ectensions de fichiers. Attention...			    
			    //  Tableau des extensions de fichiers autorisées
			    $extensionsAutorisees = array('jpeg', 'jpg', 'gif', 'png', 'bmp', 'tiff');

			    // on vérifie l'extension, ici avec le tableau des extensions possibles
			    if (!(in_array($extensionFichier, $extensionsAutorisees))) 
			    {
				$extensionOk = FALSE;
				echo "<p>Le fichier n'a pas l'extension attendue dans la liste autorisée</p>";
			    }

			    //******************************************************************************
			    //   Ou en utilisant le type MIME du fichier.
			    //   Mais il n'est pas toujours renvoyé par le client...
			    $typeFichier = filter_var($_FILES['champFichier']['type'], FILTER_SANITIZE_SPECIAL_CHARS);
			    //  Tableau des types MIME autorisées
			    $mimeAutorisees = array('image/jpeg', 'image/psd', 'image/gif', 'image/png', 'image/bmp', 'image/tiff', 'image/vnd.microsoft.icon');
			    if (!(in_array($typeFichier, $mimeAutorisees))) 
			    {
				$extensionOk = FALSE;
				echo "<p>Le fichier n'a pas un type MIME correspondant à une extension d'image</p>";
			    }
			    
			    //******************************************************************************
			    //  On peut aussi utiliser getimagesize... si on ne transfère que des images
			    list($largeur, $hauteur, $type, $attr) = getimagesize($fichierTempo); 
			    if(!(($type === IMAGETYPE_GIF) 
				    || ($type === IMAGETYPE_JPEG)
				    || ($type === IMAGETYPE_PNG) 
				    || ($type === IMAGETYPE_WBMP)))
			    {
				$extensionOk = FALSE;
				echo "<p>Le fichier n'a pas un type image</p>";
			    }

			    //  Le type de fichier semble OK, on peut valider l'upload
			    if($extensionOk)
			    {
				echo "<p>Le fichier est bien une image</p>";

				//  compose un nouveau nom, en y incluant la date
				$nomDestination = $elementsChemin['filename']."_".date("YmdHis").".".$extensionFichier;

				// on copie le fichier dans le dossier de destination (enfin!) sous son nouveau nom
				if( !move_uploaded_file($fichierTempo, $dossierDestination.$nomDestination) )
					echo "<p>Impossible de copier le fichier dans $dossierDestination</p>";

				echo "<p>Le fichier a bien été uploadé sous le nom $nomDestination</p>";
			    }
			    else
				echo "<p>Le fichier n'est pas d'un type autorisé pour le téléversement.</p>";
			}
			else
			    echo "<p>Taille du fichier incorecte</p>";
		    }
		}
	    ?>
	</fieldset>
    </div>	
    <footer id="pied">
	<p>PhAln.info for CS2i - Septembre 2017 -</p>
    </footer>
</body>
</html>