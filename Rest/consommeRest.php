<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Soleil</title>
    </head>
    <body>
	<?php
	$ville = 'lyon';
	$url = 'http://domogeek.entropialux.com/sun/'.$ville.'/all/now';
	
	//  Récupération de la chaîne json depuis l'url
	$json = file_get_contents($url);
//	var_dump($json);
	
	if($json):
	    //  Décodage vers un tableau associatif
	    $soleil = json_decode($json, true); 
//	    var_dump($soleil);
	    
	    //  Décodage vers un objet
	    $soleilObj = json_decode($json); 
//	    var_dump($soleil2);
	?>
	<h1>Version tableau</h1>
	    <p>A <?= $ville ?>, le soleil se lève à <?= $soleil['sunrise']; ?>
		et se couche à <?= $soleil['sunset']; ?>
	    </p>
	<h1>Version Objet</h1>
	    <p>A <?= $ville ?>, le soleil se lève à <?= $soleilObj->sunrise; ?>
		et se couche à <?= $soleilObj->sunset; ?>
	    </p>
	<?php else: ?>
	    <p>Ressource inaccessible.</p>			
	<?php endif; ?>	
    </body>
</html>
