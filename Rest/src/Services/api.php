<?php
require_once '../../config/config.php';

require_once './CsiService.php';
	
//  Récupération de l'url et séparation selon les /
//  Exemple d'URL:http://cni.phaln.info/services/api.obj.cni-uwpdemo.php/Mesures/Capteur/4
if(DUMP) echo '<br/>REQUEST_URI'.$_SERVER['REQUEST_URI'].'<br/>';
$requestUri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_SPECIAL_CHARS);
if(DUMP) var_dump($requestUri);

//  Séparation selon les '/'
$urlTab = explode('/', $requestUri);
if(DUMP) var_dump($urlTab);

//  Récupération du nom du script et explosion selon les /
if(DUMP) echo '<br/>SCRIPT_NAME:'.$_SERVER['SCRIPT_NAME'].'<br/>';
$scriptName = filter_input(INPUT_SERVER, 'SCRIPT_NAME', FILTER_SANITIZE_SPECIAL_CHARS);
if(DUMP) var_dump($scriptName);

$scriptNameTab = explode('/', $scriptName);
if(DUMP) var_dump($scriptNameTab);

//  On fait la différence de l'url - le nom du script pour isoler les paramètres
if(DUMP) echo '<br/>DIF<br/>';
$tabDif = array_diff($urlTab, $scriptNameTab);
if(DUMP) var_dump($tabDif);

//  Récupération du service demandé
if(DUMP) echo '<br/>ServiceFunction<br/>';
$serviceFunction = (count($tabDif) > 0) ? array_shift($tabDif) : null;
if(DUMP) var_dump($serviceFunction);

//  Positionne l'en-tête pour une réponse en json
if(!DUMP) header('Content-Type: application/json; charset=UTF-8');

//  Exécute la fonction de service demandée.
if($serviceFunction != NULL)
{
    try
    {
	//  Instanciation du service
	$service = new CsiService();

	//  Vérification de l'existance de la méthode demandée
	if(!method_exists($service, $serviceFunction))
		throw new Exception("fonction inexistante.");

	//  Vérification d'un accès en POST (en cas d'envoi de données...)
        if(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS) === 'POST')
	    $res = $service->$serviceFunction($_POST, $tabDif);
	else
	    $res = $service->$serviceFunction($tabDif);
	if(DUMP) var_dump($res);
	echo $res;
    }
    catch (Exception $e)
    {
	echo'{"Erreur":"ServiceFunction"}';
    }
}
else
{
    echo'{"Hello":"Hello from OLEN by PhAln", "Documentation":"http://quelquepart.OLEN.info/"}';    
}