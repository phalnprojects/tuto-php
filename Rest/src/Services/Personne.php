<?php
/**
 * Description d'une Personne, entité de la base de données.
 *
 * @author Philippe
 */
class Personne implements \JsonSerializable
{
    //  Les attributs, ils correspondent aux champs de la table
    protected $id_pers;
    protected $nom;
    protected $prenom;

    /**
     * Constructeur
     * Il hydrate l'objet à partir du tableau $datas.
     * @param array $data les données d'hydratation
     */
    public function __construct(array $data = NULL)
    {
	$this->hydrate($data);
    }

    /**
     * Permet "d'hydrater", c'est à dire d'affecter une valeur, à un ensemble d'attribut.
     * Elle appelle les mutateurs nécessaires.
     * @param array $datas le tableau associatif des attributs à affecter
     * @return $this l'objet courant
     */
    public function hydrate(array $datas=NULL) 
    {
	(isset($datas['id_pers'])) ? $this->setid_pers($datas['id_pers']) : $this->setid_pers(null);
	(isset($datas['nom'])) ? $this->setnom ($datas['nom']) : $this->setnom ('');
	(isset($datas['prenom'])) ? $this->setprenom ($datas['prenom']) : $this->setprenom ('');
	return $this;
    }
	
    /**
     * Surcharge du mutateur pour l'attribut $id qui ne peut pas être modifié.
     * @param int $val ne sert que si id_pers est null...
     */
    public function setId_pers($val=null)
    {
	if($this->id_pers === null)
	    $this->id_pers = ($tmp = filter_var ($val, FILTER_VALIDATE_INT)) ? $tmp : null;
    }
    
    /**
     * Les accesseurs (lecteurs et mutateurs) pour les attributs
     * 
     */
    function getId_pers() {
	return $this->id_pers;
    }
    function getId() {
	return $this->id_pers;
    }
    function setId($val=null) {
	return $this->setId_pers($val);
    }

    function getNom() {
	return $this->nom;
    }

    public function setNom($nom) {
	$this->nom = $nom;
    }

    function getPrenom() {
	return $this->prenom;
    }

    public function setPrenom($prenom) {
	$this->prenom = $prenom;
    }
    
    public function jsonSerialize(): mixed
    {
	$array = array();

	//  Récupère la liste des attributs de la classe à laquelle appartient l'objet concret
	$attrib = get_class_vars(get_class($this));
	if(DUMP) var_dump ($attrib);

	// Associe la clé du nom de l'attribut à la valeur de cet attribut
	foreach($attrib as $key=>$val)
	{
	    $get = 'get'.$key;
	    $array[$key] = $this->$get();
	}
	if(DUMP) var_dump ($array);
	return $array;
    }
    
    public function __toArray() : mixed
    {
	return $this->jsonSerialize();
    }

}