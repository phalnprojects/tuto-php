<?php
require_once './PersonneRepository.php';
require_once './Personne.php';


/**
 * Description of CsiService
 *
 * @author phaln
 */
class CsiService 
{
    public function Hazard(array $param = NULL)
    {
        if (isset($param['nb'])) {
            $tmp = filter_var($param['nb'], FILTER_VALIDATE_INT);
        }
        if($tmp !== false)
        {
            $nb = $tmp;
        } else {
            $nb = mt_rand(1, 10);
        }
	for($i=0; $i<$nb; $i++)
	{
	    $val = mt_rand(-100, 100) /10.0;
	    $res[] = [  'id' => $i,
                        'datation'=>'2017-05-05 22:22:22',
                        'val'=>$val,
                        'AppareilHasCapteurs_id'=> '7',
		     ];
	}

	if($res)
	    $json = json_encode($res);
	else
	    $json = json_encode(array());
	return $json;
    }



    public function Personne(array $param = NULL)
    {
	if(DUMP) var_dump($param);
	$res = NULL;
	
	$mapper = new PersonneRepository();
	if(!isset($param[0]))
	    $res = $mapper->getAll();
	else
	    $res = $mapper->getById((int)$param[0]);
	if(DUMP) var_dump($res);

	if($res)
	    $json = json_encode($res, JSON_UNESCAPED_UNICODE );
	else
	    $json = json_encode(array());
	if(DUMP) var_dump($json);
	return $json;
    }

    public function PostPersonne(array $post, array $param = NULL)
    {
	if(DUMP) var_dump($post);
	if(DUMP) var_dump($param);

	$arg = array(
		'id_pers' => FILTER_VALIDATE_INT,
		'nom' => FILTER_SANITIZE_SPECIAL_CHARS,
		'prenom' => FILTER_SANITIZE_SPECIAL_CHARS,
	); 
	$postFiltre = filter_var_array($post, $arg);
	
	$personne = new Personne($postFiltre);
	if(DUMP) var_dump($personne);

	$mapper = new PersonneRepository();
	$res = $mapper->sauver($personne);
	if(DUMP) var_dump($res);

	if($res)
	    $json = json_encode(array('saved' => $res), JSON_UNESCAPED_UNICODE);
	else
	    $json = json_encode(array('saved' => 'error'));
	return $json;
    }
}