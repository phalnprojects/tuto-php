<?php
require_once './Personne.php';


/**
 * Description of PersonneRepository
 * !!  NE FONCTIONNE QU'EN PDO
 * 
 * PersonneRepository est la classe qui permet de manipuler les entités Personne
 * en concordance avec la base de données.
 *
 * @author Philippe
 */
class PersonneRepository
{	
    protected $db = NULL;	    // la base de donnée (PDO ou mysqli)
    protected $table = '';	    // le nom de la table manipulée
    protected $classMapped = '';    // le nom de la classe mappée
    protected $idFieldName = 'id';  // le nom du champ clé primaire. id par défaut.

    /**
     * Constructeur.
     * Fixe le nom des attributs.
     * Attention au nom de la clé primaire: par défaut c'est 'id'.
     */
    public function __construct()
    {
	global $infoBdd;
	//  Les informations de connexion à la bdd sont dans le fichier /inc/connect.php
	$dsn = 'mysql:host='.$infoBdd['host'].';dbname='.$infoBdd['dbname'];
	$this->db = new \PDO($dsn, $infoBdd['user'], $infoBdd['pass'], array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	if(!$this->db)
	    throw new \Exceptions('Pb db dans EntityRepository::_construct()');

	$this->table = 'personne';
	$this->classMapped = 'Personne';
	$this->idFieldName = 'id_pers';
    }
	    

    /**
     * Récupère tous les enregistrements de la table.
     * @return \Phaln\Entite un tableau d'objet de classe classMapped dérivant Entite
     * @throws RepositoryException
     */
    public function getAll()
    {
        $resultSet = NULL;
	$query = 'SELECT * FROM '.$this->table;

	$rqtResult = $this->db->query($query);
	if($rqtResult)
	{
	    $cla = get_class($this->db);
	    while($row = $rqtResult->fetch(\PDO::FETCH_ASSOC))
	    {
		$resultSet[] = new Personne($row);
	    }
	}	
        return $resultSet;
    }
	
    /**
     * Récupère l'enregistrement n° $id dans la table
     * @param type $id la valeur de la pk à récupérer
     * @return \Phaln\Entite un objet de classe classMapped dérivant Entite
     */
    public function getById($id)
    {
        $resultSet = NULL;
	
	$query = 'SELECT * FROM '.$this->table .
			' WHERE '.$this->idFieldName.' = :idFieldName';
	$reqPrep = $this->db->prepare($query);
	$reqPrep->bindValue(':idFieldName', $id);
	$reqPrep->execute();

	while($row = $reqPrep->fetch(\PDO::FETCH_ASSOC))
	{
	    $resultSet[] = new Personne($row);
	}
        return $resultSet;
    }
    
    /**
     * Enregistre l'entité dans la table.
     * Gère les cas d'une nouvelle entité (id est null => insertion) ou
     * d'une entité existante (id != null => update)
     * @param Entite $entity
     * @return Entite
     * @throws RepositoryException
     */
    public function sauver(Personne $entity)
    {
         $resultSet = NULL;
	if($entity != NULL)
	{
	    $bindParam = $entity->__toArray();
	    if($entity->getid() == NULL)
	    {
		// Nouvelle entité
		$query = "INSERT INTO $this->table" .
			" (`$this->idFieldName`, `nom`, `prenom`)"
			. " VALUES (NULL, :nom, :prenom)";

		$reqPrep = $this->db->prepare($query);
		$reqPrep->bindParam(':nom', $bindParam['nom'], \PDO::PARAM_STR);
		$reqPrep->bindParam(':prenom', $bindParam['prenom'], \PDO::PARAM_STR);
		$reqPrep->execute();
		
		if($reqPrep != FALSE)
		{
		    $entity->setid($this->db->lastInsertId());
		    $resultSet = $entity;
		}
	    }
	    else
	    {
		//  Entité existante
		$query = "UPDATE $this->table"
			. " SET `nom` = :nom, `prenom` = :prenom"
			. " WHERE `$this->idFieldName` = :id";
		
		$reqPrep = $this->db->prepare($query);
		$reqPrep->bindParam(':nom', $bindParam['nom'], \PDO::PARAM_STR);
		$reqPrep->bindParam(':prenom', $bindParam['prenom'], \PDO::PARAM_STR);
		$reqPrep->bindParam(':id', $bindParam[$this->idFieldName], \PDO::PARAM_INT);

		$reqPrep->execute();

		if($reqPrep != FALSE)
		{
		    $resultSet = $entity;
		}
	    }
	}
        return $resultSet;	
    }
    
}
