<?php
// Url de base de l'application
define('URL_BASE', "http://localhost/dev/TutosPhp/Rest/");
// Le chemin, à partir de l'url de base, dans lequel chercher les classes.
define('CLASS_DIR', 'src/');

// Mise en place d'un autoload des classes, pour éviter les require / include des fichiers
// 1) le chemin des classe
set_include_path(get_include_path(). PATH_SEPARATOR . CLASS_DIR);
// 2) les extensions possibles pour les fichiers des classes
spl_autoload_extensions('.php');
// 3) Enregistrement auprès de PHP des consognes de chargement des classes
spl_autoload_register();


//  Les informations de connexion à la BDD
require_once 'connect.php';

 
//  Nécessaire pour éviter le "bug" du json_encode qui travaille différement
//  avec les float (pb de précision) à partir de PHP7.1 
if (version_compare(phpversion(), '7.1', '>=')) {
    ini_set( 'serialize_precision', -1 );
}


// Pour tracer les var_dump dans vos fichiers, définissez DUMP à TRUE
define('DUMP', false);
if(DUMP)
{
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors','On');  
}
else
{
    ini_set('display_errors','Off');  
}

// Remplace la fonction var_dump si elle pose des problèmes d'affichage
// (c'est le cas si XDebug n'est pas installé)
function dump_var($var, $dump=DUMP, $msg=null)
{
    if($dump) {
	if($msg)
	    echo"<p><strong>$msg</strong></p>";
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
    }
}