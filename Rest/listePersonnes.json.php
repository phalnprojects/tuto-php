<?php

//  Les données, issues d'une bdd par exemple...
$personnes = [
    array(
	'nom' => 'Onet',
	'prenom' => 'Camille',
	'age' => 25
    ),
    array(
	'nom' => 'Honxa',
	'prenom' => 'Cécile',
	'age' => 23
    )
];

//  Composition du résultat à retourner (l'ensemble des données)
$tabLivre = array(
    'titre' => 'Ma vie en SNIR',
    'auteurs' => $personnes
);

//  Encodage en json du résultat
//  {"tites":"Ma vie en SNIR","auteurs":[{"nom":"Onet","prenom":"Camille","age":25},{"nom":"Honxa","prenom":"Cécile","age":23}]}
$json = json_encode($tabLivre);

//  Ne pas oublier!!
header('Content-Type: application/json');

//  Ecriture du json dans le flux de sortie
echo $json;
