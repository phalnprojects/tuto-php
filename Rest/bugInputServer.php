<?php
// Sur certaines configurations avec PHP installé en FastCGI,
// filter_input ne fonctionne pas correctement avec INPUT_SERVER... 
$requestUri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_SPECIAL_CHARS);

// Vous pouvez vérifier l'effet à l'url suivante: http://cni.phaln.info/inc/tests/testFilterInput.php?varget=TestGet
// Source: http://php.net/manual/es/function.filter-input.php#77307
// Voici un correctif. Source: https://github.com/xwp/stream/issues/254
if (filter_has_var(INPUT_SERVER, "REQUEST_URI")) {
       // $requestUri = filter_input(INPUT_SERVER, "REQUEST_URI", FILTER_UNSAFE_RAW, FILTER_NULL_ON_FAILURE);
        $requestUri = filter_input(INPUT_SERVER, "REQUEST_URI", FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
} else {
	if (isset($_SERVER["REQUEST_URI"]))
		$requestUri = filter_var($_SERVER["REQUEST_URI"], FILTER_SANITIZE_SPECIAL_CHARS, FILTER_NULL_ON_FAILURE);
	else
		$requestUri = null;
}
