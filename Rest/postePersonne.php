<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Poste Personne</title>
    </head>
    <body>
	<?php
	try {
	    require_once './config/config.php';
	    $url = URL_BASE . 'src/Services/api.php/PostPersonne';
	    var_dump($url);

	    $data = array('nom' => 'Woman', 'prenom' => 'New');
	    // utilisez 'http' même si vous envoyez la requête sur https:// ...
	    $options = array(
		'http' => array(
		    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
		    'method' => 'POST',
		    'content' => http_build_query($data)
		)
	    );
	    var_dump($options);
	    $context = stream_context_create($options);
	    var_dump($context);
	    $result = file_get_contents($url, false, $context);
	    if ($result === FALSE) {
		echo "<p>Oups...</p>";
	    }
	    $retour = json_decode($result, true);
	    var_dump($retour);
	} catch (Throwable $e) {
	    var_dump($e);
	}
	?>
    </body>
</html>
